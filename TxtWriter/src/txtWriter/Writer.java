package txtWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Writer {

	public static void writeToFile(File file, String data) {
		try {
			FileOutputStream is = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(is);
			BufferedWriter w = new BufferedWriter(osw);
			w.write(data);
			w.close();
		} catch (IOException e) {
			System.err.println("Problem writing to the file!");
		}
	}

	public static void main(String[] args) {
		File testShares = new File(
				"C:/Users/Alex/workspace/Marktanalyse/testShares.txt");
		File testWorld = new File(
				"C:/Users/Alex/workspace/Game of Life/testWorld.txt");
		writeToFile(testShares, DataCreater.createRandomShareData(20, 100));
		writeToFile(testWorld, DataCreater.createRandomWorldData(100, 100));
	}

}
