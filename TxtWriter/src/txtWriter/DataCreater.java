package txtWriter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

public class DataCreater {

	private static int randomInt(int low, int high) {
		high++;
		return (int) (Math.random() * (high - low) + low);
	}

	private static double randomDouble(double low, double high) {
		high++;
		return (Math.random() * (high - low) + low);
	}

	private static String generateRandomString(int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append((char) randomInt(97, 122));
		}
		return sb.toString();
	}

	public static String createRandomShareData(int anzahlShares,
			int anzahlDataProShare) {
		final HashSet<LocalDate> hs = new HashSet<>();
		final StringBuilder sb = new StringBuilder();
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
		dfs.setDecimalSeparator('.');
		DecimalFormat deciFormat = new DecimalFormat("0.00", dfs);
		DateTimeFormatter dateFormat = DateTimeFormatter
				.ofPattern("dd.MM.yyyy");

		sb.append("date;open;high;low;close");
		sb.append(System.getProperty("line.separator"));
		for (int i = 0; i < anzahlShares; i++) {
			sb.append("SHARE;" + generateRandomString(12) + ";;;");
			sb.append(System.getProperty("line.separator"));
			hs.clear();
			for (int j = 0; j < anzahlDataProShare; j++) {
				try {
					LocalDate date = LocalDate.of(randomInt(2000, 2013),
							randomInt(1, 12), randomInt(1, 31));
					while (hs.contains(date)) {

						date = LocalDate.of(randomInt(2000, 2013),
								randomInt(1, 12), randomInt(1, 31));
					}
					double high = randomDouble(550, 1000);
					double low = randomDouble(500, high - 50);
					double open = randomDouble(low, high);
					double close = randomDouble(low, high);

					sb.append(dateFormat.format(date) + ";"
							+ deciFormat.format(open) + ";"
							+ deciFormat.format(high) + ";"
							+ deciFormat.format(low) + ";"
							+ deciFormat.format(close));
					sb.append(System.getProperty("line.separator"));
					hs.add(date);
				} catch (DateTimeException e) {
					j--;
					continue;
				}
			}

		}
		return sb.toString();
	}

	public static String createRandomWorldData(int hoehe, int breite) {
		StringBuilder sb = new StringBuilder();

		sb.append(breite + "x" + hoehe);
		for (int i = 0; i < hoehe; i++) {
			sb.append(System.getProperty("line.separator"));
			for (int j = 0; j < breite; j++) {
				sb.append(randomInt(0, 1));
			}
		}
		return sb.toString();

	}
}
