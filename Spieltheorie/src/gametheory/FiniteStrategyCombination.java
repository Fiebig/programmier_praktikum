package gametheory;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

/**
 * Stellt eine Strategiekombination in einem Spiel mit endlich vielen endlichen
 * Mitspielern dar.
 * 
 * @author Alexander Fiebig
 * 
 * @param <U>
 *            Typ der Spieler in der Strategiekombination
 * @param <T>
 *            Typ der Strategien in der Strategiekombination
 */
public class FiniteStrategyCombination<T extends FiniteProfitPlayer<T, U>, U extends Strategy<U, T>> {
	/**
	 * Speichert, welchem Spieler welche seiner Strategien zugeordnet ist.
	 */
	protected final TreeMap<T, U> combination;

	/**
	 * Initialisiert eine leere Instanz
	 */
	public FiniteStrategyCombination() {
		this.combination = new TreeMap<>();
	}

	/**
	 * Copy Constructor.
	 * 
	 * @param other
	 */
	public FiniteStrategyCombination(FiniteStrategyCombination<T, U> other) {
		this.combination = new TreeMap<>(other.combination);
	}

	/**
	 * Ersetzt die Strategie des Spielers, der s zugeordnet ist, durch s.
	 * 
	 * @param strategy
	 *            Strategie,die dem Spieler neu zugeordnet wird
	 */
	public void putStrategyForPlayer(U strategy) {
		combination.put(strategy.getRelatedPlayer(), strategy);
	}

	/**
	 * Gibt die Strategie zur�ck, die player zugeordnet ist oder null falls
	 * player keine Strategie zugeordnet ist.
	 * 
	 * @param player
	 *            Spieler
	 * @return Strategie, die player zugeordnet ist.
	 */
	public U getStrategyForPlayer(T player) {
		return combination.get(player);
	}

	/**
	 * "Player i" ist der Name des i. Spielers und "Strategie von Player i" ist
	 * der Name der von ihm verwendeten Strategie.
	 * 
	 * @return (Player 1:Strategie von Player 1,..., Player N:Strategie von
	 *         Player N)
	 */
	@Override
	public String toString() {
		Set<T> s = combination.keySet();
		StringBuilder sb = new StringBuilder(s.size() + 2);
		sb.append('(');
		for (Iterator<T> i = s.iterator(); i.hasNext();) {
			T temp = i.next();
			sb.append(temp.getName() + ":" + combination.get(temp).getName()
					+ ",");
		}
		sb.deleteCharAt(sb.lastIndexOf(","));
		sb.append(')');
		return sb.toString();
	}

	/**
	 * Gibt genau dann true zur�ck, wenn diese Strategiekombination ein
	 * Nash-Equilibrium ist. Ein Nash Equilibrium liegt genau dann vor, wenn es
	 * sich in dieser Strategiekombination f�r keinen Spieler lohnt seine
	 * Strategie zu �ndern.
	 * 
	 * @return true, wenn diese Strategiekombination ein Nash-Equilibrium ist
	 */
	public boolean isNashEquilibrium() {
		// Kopie erstellen
		FiniteStrategyCombination<T, U> fscCopy = new FiniteStrategyCombination<>(
				this);

		for (T spielerTemp : combination.keySet()) {
			for (U stratTemp : spielerTemp.getStrategies()) {
				fscCopy.putStrategyForPlayer(stratTemp);
				if (spielerTemp.isBetter(fscCopy, this)) {
					return false;
				}
			}
			fscCopy.putStrategyForPlayer(this.getStrategyForPlayer(spielerTemp));
		}
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FiniteStrategyCombination<?, ?> other = (FiniteStrategyCombination<?, ?>) obj;
		if (combination == null) {
			if (other.combination != null) {
				return false;
			}
		}
		else if (!combination.equals(other.combination)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((combination == null) ? 0 : combination.hashCode());
		return result;
	}

}
