package gametheory;

import java.io.Serializable;

/**
 * Die Player-Klasse dient als ganz allgemeine Spielerklasse. Sie dient als
 * Oberklasse f�r diverse verschiedene Spielertypen. Direkt erweitert werden
 * sollte sie nur f�r Spieler mit unendlich vielen Strategien.
 * 
 * @author Alexander Fiebig
 * 
 * @param <U>
 *            Laufzeit-Typ dieses Spielers
 * @param <T>
 *            Typ der Strategien dieses Spielers
 * 
 */
public abstract class Player<U extends Player<U, T>, T extends Strategy<T, U>> implements Serializable, Comparable<U> {

	private static final long serialVersionUID = 1L;
	/**
	 * Name des Spielers.
	 */
	protected String name;
	/**
	 * Die aktuell gew�hlte Strategie des Spielers
	 */
	protected T currentStrategy;

	/**
	 * Initialisiert eine Instanz einer Subklasse von Player
	 * 
	 * @param name
	 *            Name des Spielers
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * Liefert den Namen des Spielers
	 * 
	 * @return Name des Spielers
	 */
	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Player<?, ?> other = (Player<?, ?>) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Diese Methode vergleicht zwei Spieler anhand ihrer Namen, ein Spieler A
	 * ist z.B. "kleiner" als ein anderer Spieler B, wenn der Name von A vor dem
	 * von B im Alphabet kommt.
	 */
	@Override
	public int compareTo(U other) {
		if (other == null) {
			throw new NullPointerException("Fehlendes Argument");
		}
		return name.compareTo(other.getName());
	}

	/**
	 * Liefert den Namen des Spielers
	 * 
	 * @return Name des Spielers
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * Liefert die aktuelle Strategie des Spielers.
	 * 
	 * @return
	 */
	public T getCurrentStrategy() {
		return currentStrategy;
	}

	/**
	 * Setzt die aktuell zu verwendente Strategie des Spielers
	 * 
	 * @param currentStrategy
	 * @throws IllegalArgumentException
	 *             falls die gegebene Strategie s nicht zu diesem Player geh�rt
	 */
	public void setCurrentStrategy(T currentStrategy) {
		if (!currentStrategy.getRelatedPlayer().equals(this)) {
			throw new IllegalArgumentException(
					"Strategie ist einem anderen Spieler zugeordnet");
		}
		this.currentStrategy = currentStrategy;
	}

}
