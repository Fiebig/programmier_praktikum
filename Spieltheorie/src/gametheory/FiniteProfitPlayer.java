package gametheory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Subklasse von Player, die f�r einen Spieler mit endlich vielen Strategien,
 * der gezielt auf Profit spielt, steht.
 * 
 * @author Alexander Fiebig
 * 
 * @param <U>
 *            Laufzeit-Typ dieses Spielers
 * @param <T>
 *            Typ der Strategien dieses Spielers
 */
public abstract class FiniteProfitPlayer<U extends FiniteProfitPlayer<U, T>, T extends Strategy<T, U>> extends Player<U, T> {

	private static final long serialVersionUID = 1L;
	/**
	 * Speichert die Strategien des Spielers
	 */
	protected final List<T> strategies;
	/**
	 * Klasse, die die Schnittstelle ProfitFunction implementiert.
	 */
	protected ProfitFunction<U, T> profitComputer;

	/**
	 * Initialisiert eine Instanz
	 * 
	 * @param name
	 *            Name des Spielers
	 */
	public FiniteProfitPlayer(String name) {
		super(name);
		this.strategies = new ArrayList<>();
	}

	/**
	 * Initialisiert eine Instanz mit gegebenen Strategien
	 * 
	 * @param name
	 *            Name des Spielers
	 * @param strategies
	 *            Strategien des Spielers
	 * 
	 */
	public FiniteProfitPlayer(String name, Collection<T> strategies) {
		super(name);
		this.strategies = new ArrayList<>(strategies);
	}

	/**
	 * Liefert eine Liste mit den Strategien des Spielers
	 * 
	 * @return Strategien des Spielers
	 */
	public List<T> getStrategies() {
		return strategies;
	}

	/**
	 * Setzt die Strategien des Spielers.
	 * 
	 * @param strategies
	 *            Strategien, die der Spieler haben soll
	 * 
	 * @throws IllegalArgumentException
	 *             falls versucht wird eine Strategie zum Spieler hinzuzuf�gen,
	 *             die zu einem anderen Spieler geh�rt
	 */
	public void setStrategies(Collection<T> strategies) {
		strategies.clear();
		for (Iterator<T> i = strategies.iterator(); i.hasNext();) {
			addStrategy(i.next());
		}
	}

	/**
	 * F�gt dem Spieler eine Strategie hinzu, wobei einem Spieler eine Strategie
	 * h�chstens einmal hinzugef�gt wird.
	 * 
	 * @param strategy
	 *            Neue Strategie des Spielers
	 * 
	 * @throws IllegalArgumentException
	 *             falls strategy nicht zu diesem Player geh�rt
	 */
	public void addStrategy(T strategy) {
		if (!strategy.getRelatedPlayer().equals(this)) {
			throw new IllegalArgumentException(
					"Strategie ist einem anderen Spieler zugeordnet");
		}
		if (strategies.contains(strategy) == false) {
			strategies.add(strategy);
		}
	}

	/**
	 * Enfernt die gegebene Strategie von den Strategien des Spielers, falls
	 * diese dabei ist.
	 * 
	 * @param strategy
	 */
	public void removeStrategy(T strategy) {
		strategies.remove(strategy);
	}

	/**
	 * Setzt einen ProfitComputer f�r den Spieler.
	 * 
	 * @param pf
	 *            ProfitComputer f�r den Spieler
	 */
	public void setProfitFunction(ProfitFunction<U, T> pf) {
		profitComputer = pf;
	}

	/**
	 * Hilfsfunktion, returns this.
	 * 
	 * @return
	 */
	protected abstract U getThis();

	/**
	 * Liefert alle Strategien zur�ck, die auf die gegebene Strategiekombination
	 * die beste Antwort dieses Spielers darstellen. Eine beste Antwort eines
	 * Spielers auf eine Strategiekombination ist dabei eine Strategie, die den
	 * Profit des Spielers maximiert, wenn man davon ausgeht, dass alle anderen
	 * Spieler nicht von ihrer Strategie abweichen.
	 * 
	 * @param fsc
	 *            Gegebene Strategiekombination
	 * 
	 * @throws NullPointerException
	 *             falls f�r den Player noch kein ProfitComputer festgelegt
	 *             wurde
	 * @return Liste mit den besten Strategien f�r den Spieler unter der
	 *         gegebenen Strategiekombination oder null falls momentan keine
	 *         Strategie verf�gbar
	 */
	public List<T> bestReplies(FiniteStrategyCombination<U, T> fsc) {
		if (profitComputer == null) {
			throw new NullPointerException("F�r Spieler " + name
					+ " wurde noch keine ProfitFunction festgelegt!");
		}
		FiniteStrategyCombination<U, T> fscCopy = new FiniteStrategyCombination<>(
				fsc);
		double maxBewertung = Double.NEGATIVE_INFINITY;
		final HashMap<T, Double> bewertungsMap = new HashMap<>(
				strategies.size());
		for (T stratTemp : strategies) {
			fscCopy.putStrategyForPlayer(stratTemp);
			double bewertungsTemp = profitComputer.computeProfit(getThis(),
					fscCopy);
			bewertungsMap.put(stratTemp, bewertungsTemp);
			if (bewertungsTemp > maxBewertung) {
				maxBewertung = bewertungsTemp;
			}
		}
		// Pr�fen ob Spieler momentan �berhaupt Strategie als Antwort zur
		// Verf�gung hat
		if (maxBewertung != Double.NEGATIVE_INFINITY) {
			List<T> results = new LinkedList<>();
			for (T stratTemp : strategies) {
				double bewertungsTemp = bewertungsMap.get(stratTemp);
				if (bewertungsTemp == maxBewertung) {
					results.add(stratTemp);
				}
			}
			return results;
		}
		return null;
	}

	/**
	 * Liefert genau dann true zur�ck, wenn die Strategiekombination fsc1 besser
	 * f�r diesen Player ist, als die Strategiekombination fsc2. Welche
	 * Strategiekombinationen dabei jeweils besser ist, wird durch
	 * profitFunction festgelegt.
	 * 
	 * @param fsc1
	 *            Strategiekombination zum Vergleich
	 * @param fsc2
	 *            Strategiekombination zum Vergleich
	 * 
	 * @throws NullPointerException
	 *             falls f�r den Player noch keine ProfitFunction festgelegt
	 *             wurde
	 * 
	 * @return true, wenn fsc1 besser f�r diesen Player ist, als fsc2.
	 * 
	 */
	public boolean isBetter(FiniteStrategyCombination<U, T> fsc,
			FiniteStrategyCombination<U, T> fsc2) {
		if (profitComputer == null) {
			throw new NullPointerException("F�r Spieler " + name
					+ " wurde noch keine ProfitFunction festgelegt!");
		}
		return profitComputer.computeProfit(getThis(), fsc) > profitComputer
				.computeProfit(getThis(), fsc2);
	}

	/**
	 * "Spielername" ist der Name des Spielers und
	 * "(Strategie1, ..., StrategieN)" die Namen seiner Strategien.
	 * 
	 * @return Spielername(Strategie1, ... , StrategieN)
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(strategies.size() + 2);
		sb.append(name + "(");
		for (T strat : strategies) {
			sb.append(strat.getName() + ",");
		}
		sb.deleteCharAt(sb.lastIndexOf(","));
		sb.append(")");
		return sb.toString();
	}

}
