package gametheory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Steht f�r ein Spiel mit endlich vielen Mitspielern.
 * 
 * @author Alexander Fiebig
 * 
 *
 * @param <T>
 *            Typ der Spieler dieses Spiels
 * @param <U>
 *            Typ der Strategien der Spieler dieses Spiels
 */
public abstract class FiniteGame<T extends FiniteProfitPlayer<T, U>, U extends Strategy<U, T>>
		implements
			Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Speichert die Spieler, die am Spiel beteiligt sind
	 */
	protected final List<T> players;
	protected T spielerAmZug;

	/**
	 * Initialisiert eine Instanz
	 */
	public FiniteGame() {
		this.players = new ArrayList<>();
	}
	/**
	 * Initialisiert eine Instanz mit gegebenen Spielern
	 * 
	 * @param players
	 */
	public FiniteGame(Collection<T> players) {
		this.players = new ArrayList<>(players);
	}

	/**
	 * Liefert die Spieler, die am Spiel beteiligt sind
	 * 
	 * @return Liste der Spieler, die am Spiel beteiligt sind
	 */
	public List<T> getPlayers() {
		return players;
	}

	/**
	 * F�gt einen weiteren Spieler zum Spiel hinzu.
	 * 
	 * @param p
	 *            Neuer Spieler
	 */
	public void addPlayer(T player) {
		if (!(players.contains(player))) {
			players.add(player);
		}
	}
	/**
	 * Enfernt den gegebenen Spieler vom Spiel, falls dieser dabei ist.
	 * 
	 * @param player
	 */
	public void removePlayer(T player) {
		players.remove(player);
	}
	/**
	 * Setzt die Spieler des Spiels.
	 * 
	 * @param players
	 *            Spieler, die am Spiel teilnehmen sollen
	 * 
	 */
	public void setPlayers(Collection<T> players) {
		players.clear();
		for (Iterator<T> i = players.iterator(); i.hasNext();) {
			addPlayer(i.next());
		}
	}

	/**
	 * Liefert den spielerAmZug.
	 * 
	 * @return spielerAmZug
	 */
	public T getSpielerAmZug() {
		return spielerAmZug;
	}
	/**
	 * Setzt den spielerAmZug.
	 * 
	 * @param spielerAmZug
	 */
	public void setSpielerAmZug(T spielerAmZug) {
		this.spielerAmZug = spielerAmZug;
	}

	/**
	 * Sucht unter allen m�glichen Strategiekombinationen der Mitspieler ein
	 * Nash-Equilibrium und gibt ein solches zur�ck. Falls es kein
	 * Nash-Equilibrium gibt, so liefert die Methode null zur�ck.
	 * 
	 * @throws NullPointerException
	 *             falls f�r einen der Spieler dieses Spiels weder ein
	 *             ProfitComputer noch eine PreferenceRelation festgelegt wurde
	 * @return Nash-Equilibrium falls ein solches existiert, sonst null
	 */
	public final FiniteStrategyCombination<T, U> computeNashEquilibrium() {
		final List<U> allStrategies = new ArrayList<>();
		final FiniteStrategyCombination<T, U> fsc = new FiniteStrategyCombination<>();

		for (T spielerTemp : players) {
			List<U> spielerStrategies = spielerTemp.getStrategies();
			allStrategies.addAll(spielerStrategies);
			if (spielerStrategies.isEmpty() == false) {
				fsc.putStrategyForPlayer(spielerStrategies.get(0));
			}
		}

		for (U stratTempOuter : allStrategies) {
			fsc.putStrategyForPlayer(stratTempOuter);
			for (U stratTempInner : allStrategies) {
				if (stratTempOuter.getRelatedPlayer().equals(
						stratTempInner.getRelatedPlayer()) == false) {
					fsc.putStrategyForPlayer(stratTempInner);
					if (fsc.isNashEquilibrium()) {
						return fsc;
					}
				}
			}
		}
		return null;
	}

}