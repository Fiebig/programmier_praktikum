package gametheory;

import java.io.Serializable;

/**
 * Eine Strategie hat einen Namen und ist fest einem Spieler zugeordnet.
 * 
 * @author Alexander Fiebig
 * 
 */
public class Strategy<U extends Strategy<U, T>, T extends Player<T, U>> implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Name der Strategie.
	 */
	protected final String name;
	/**
	 * Spieler, der der Strategie zugeordnet ist.
	 */
	protected final T relatedPlayer;

	/**
	 * Initialisiert eine Instanz. Jede Strategie hat einen Namen und ist fest
	 * einem Spieler zugeordnet.
	 * 
	 * @param name
	 *            Name der Strategie
	 * @param relatedPlayer
	 *            Spieler, der der Strategie zugeordnet ist
	 */
	public Strategy(String name, T relatedPlayer) {
		this.name = name;
		this.relatedPlayer = relatedPlayer;
	}

	/**
	 * Liefert den Namen der Srategie.
	 * 
	 * @param name
	 *            Name der Srategie
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert den Spieler, der dieser Stategie zugeordnet ist.
	 * 
	 * @return Spieler, der dieser Stategie zugeordnet ist
	 */
	public T getRelatedPlayer() {
		return relatedPlayer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Strategy<?, ?> other = (Strategy<?, ?>) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
