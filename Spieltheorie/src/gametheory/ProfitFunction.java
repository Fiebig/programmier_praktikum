package gametheory;

import java.io.Serializable;

/**
 * In einem Gewinnspiel l�sst sich der Nutzen, den ein Spieler aus einer
 * Strategiekombination ziehen kann , konkret als numerischer Wert angeben. Eine
 * Klasse, die dieses Interface implementiert, liefert dadurch direkt eine
 * Methode zur numerischen Bewertung einer Strategiekombination.
 * 
 * @author Alexander Fiebig
 *
 * @param <U>
 *            Typ der Spieler der zu bewertenden Strategiekombination
 * @param <T>
 *            Typ der Strategien der zu bewertenden Strategiekombination
 */
public interface ProfitFunction<U extends FiniteProfitPlayer<U, T>, T extends Strategy<T, U>> extends Serializable {
	/**
	 * Bewertet fsc numerisch
	 * 
	 * @param player
	 *            Spieler f�r den Profit berechnet wird
	 * @param fsc
	 *            Strategiekombination zum Bewerten
	 * @return Numerische Bewertung von fsc
	 */

	public double computeProfit(U player, FiniteStrategyCombination<U, T> fsc);
}
