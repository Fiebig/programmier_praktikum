package turingMachine;

import turingMachine.tape.Direction;
import turingMachine.tape.MultiTape;
import turingMachine.tape.MultiTapeReadWriteData;
import finiteStateMachine.AbstractFiniteStateMachine;

/**
 * Erweitert die Klasse
 * {@code AbstractFiniteStateMachine<MultiTapeReadWriteData<T>,
 * TuringTransitionOutput<T>>}. Hat nur einen generischen Parameter, denn
 * Eingabe- und Ausgabealphabet muessen gleich sein, da von den selben Baendern
 * gelesen wird, auf die geschrieben wird.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der Turing-Maschine
 */
public class TuringMachine<T> extends AbstractFiniteStateMachine<MultiTapeReadWriteData<T>, TuringTransitionOutput<T>> {
	/**
	 * Baender der Turing-Maschine.
	 */
	private final MultiTape<T> baender;

	/**
	 * Erstellt eine Turing-Maschine mit tapeCount Baendern.
	 * 
	 * @param tapeCount
	 *            Anzahl der Baender der Maschine
	 * 
	 * @throws IllegalArgumentException
	 *             falls tapeCount <= 0 ist
	 */
	public TuringMachine(int tapeCount) {
		if (tapeCount <= 0) {
			throw new IllegalArgumentException("Ungueltige Bandanzahl");
		}
		baender = new MultiTape<T>(tapeCount);
	}

	/**
	 * Fuehrt so lange Zustandsuebergaenge aus, bis die Maschine einen
	 * akzeptierten Zustand erreicht hat oder der Fall eintritt, dass kein
	 * Zustandsuebergang definiert ist.
	 * 
	 * @throws IllegalStateException
	 *             falls kein Zustand als aktuell gekennzeichnet ist
	 */
	public void run() {
		while (!isInAcceptedState()) {
			try {
				transit();
			}
			catch (IllegalArgumentException ex) {
				System.out.println(ex.getMessage());
				break;
			}
		}
	}

	/**
	 * Fuehrt basierend auf den aktuell von den Baendern gelesenen Daten und dem
	 * aktuellen Zustand einen Zustandsuebergang aus.
	 * 
	 * @throws IllegalStateException
	 *             falls sich die Maschine bereits in einem akzeptierten Zustand
	 *             befindet oder kein Zustand als aktuell gekennzeichnet ist
	 * @throws IllegalArgumentException
	 *             falls kein Uebergang für die Eingabe aus den Baendern
	 *             definiert ist
	 */
	public void transit() {
		if (isInAcceptedState()) {
			throw new IllegalStateException(
					"Die Maschine befindet sich bereits in einem aktzeptierten Zustand");
		}
		super.transit(baender.read());
	}

	/**
	 * Liefert die Anzahl der Baender der Maschine.
	 * 
	 * @return Anzahl der Baender der Maschine
	 */
	public int getTapeCount() {
		return baender.getTapeCount();
	}

	/**
	 * Liefert die Baender der Maschine.
	 * 
	 * @return Baender der Maschine
	 */
	public MultiTape<T> getTapes() {
		return baender;
	}

	/**
	 * Zunaechst werden die neuen Daten auf die Baender geschrieben und
	 * anschließend die Lese-/Schreibkoepfe in die entsprechenden Richtungen
	 * bewegt.
	 */
	@Override
	public void processOutput(TuringTransitionOutput<T> output) {
		baender.write(output.getToWrite());
		baender.move(output.getDirections());
	}

	/**
	 * Beispiel:<br>
	 * Erstellt eine Turing-Maschine mit 2 Baendern. Auf das erste Band wird das
	 * Alphabet von a bis m, beginnend mit a, geschrieben. Auf das zweite Band
	 * wird das Alphabet von n bis z, beginnend mit z, geschrieben.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		TuringMachine<Character> alphabet = new TuringMachine<Character>(2);
		createStates(alphabet);
		createTransitions(alphabet);
		// Startzustand
		alphabet.setCurrentState("1");
		alphabet.run();
		alphabet.setCurrentState("1");
		// Ausgabe der Baender
		System.out.println(alphabet.getTapes().toString());

	}

	/**
	 * Erzeugt die Zustaende fuer das Beispiel.
	 * 
	 * @param alphabet
	 */
	private static void createStates(TuringMachine<Character> alphabet) {
		alphabet.addState("1");
		alphabet.addState("2");
		alphabet.addState("3");
		alphabet.addState("4");
		alphabet.addState("5");
		alphabet.addState("6");
		alphabet.addState("7");
		alphabet.addState("8");
		alphabet.addState("9");
		alphabet.addState("10");
		alphabet.addState("11");
		alphabet.addState("12");
		alphabet.addState("13");
		alphabet.addState("14");
		alphabet.getState("14").setAccepted(true);

	}

	/**
	 * Erzeugt die Uebergaenge fuer das Beispiel.
	 * 
	 * @param alphabet
	 */
	private static void createTransitions(TuringMachine<Character> alphabet) {
		/*
		 * Erstellung des Input-Objekts fuer die Uebergaenge (ist fuer alle
		 * Uebergaenge gleich). Ist leer, da Koepfe immer ueber einer leeren
		 * Zelle sind.
		 */
		final MultiTapeReadWriteData<Character> input = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		// Uebergang 1
		MultiTapeReadWriteData<Character> dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'a');
		dataForOutput.set(1, 'z');
		TuringTransitionOutput<Character> output = new TuringTransitionOutput<Character>(
				dataForOutput, Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("1", input, "2", output);
		// Uebergang 2
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'b');
		dataForOutput.set(1, 'y');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("2", input, "3", output);
		// Uebergang 3
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'c');
		dataForOutput.set(1, 'x');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("3", input, "4", output);
		// Uebergang 4
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'd');
		dataForOutput.set(1, 'w');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("4", input, "5", output);
		// Uebergang 5
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'e');
		dataForOutput.set(1, 'v');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("5", input, "6", output);
		// Uebergang 6
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'f');
		dataForOutput.set(1, 'u');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("6", input, "7", output);
		// Uebergang 7
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'g');
		dataForOutput.set(1, 't');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("7", input, "8", output);
		// Uebergang 8
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'h');
		dataForOutput.set(1, 's');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("8", input, "9", output);
		// Uebergang 9
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'i');
		dataForOutput.set(1, 'r');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("9", input, "10", output);
		// Uebergang 10
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'j');
		dataForOutput.set(1, 'q');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("10", input, "11", output);
		// Uebergang 11
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'k');
		dataForOutput.set(1, 'p');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("11", input, "12", output);
		// Uebergang 12
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'l');
		dataForOutput.set(1, 'o');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("12", input, "13", output);
		// Uebergang 13
		dataForOutput = new MultiTapeReadWriteData<Character>(
				alphabet.getTapeCount());
		dataForOutput.set(0, 'm');
		dataForOutput.set(1, 'n');
		output = new TuringTransitionOutput<Character>(dataForOutput,
				Direction.RIGHT, Direction.LEFT);
		alphabet.addTransition("13", input, "14", output);

	}

}
