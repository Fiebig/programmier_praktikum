package turingMachine;

import turingMachine.tape.Direction;
import turingMachine.tape.MultiTapeReadWriteData;
/**
 * Stellt die Ausgabe einer Turing-Maschine dar, die sich aus den Werten, die
 * auf die einzelnen Baender geschrieben werden (MultiTapeReadWriteData) und den
 * Richtungen, in die die einzelnen Lese-/Schreibkoepfe der Baender bewegt
 * werden sollen (Direction[]), zusammensetzt.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der zugehoerigen Turing-Maschine
 */
public class TuringTransitionOutput<T> {
	/**
	 * Die zu schreibenden Daten.
	 */
	private final MultiTapeReadWriteData<T> toWrite;
	/**
	 * Bewegungsrichtungen der einzelnen Koepfe.
	 */
	private final Direction[] directions;
	/**
	 * Erstellt eine Uebergangsausgabe mit den zu schreibenden Daten und den
	 * Bewegungsrichtungen der einzelnen Koepfe.
	 * 
	 * @param toWrite
	 *            Die zu schreibenden Daten
	 * @param directions
	 *            Bewegungsrichtungen der einzelnen Koepfe
	 * 
	 * @throws IllegalArgumentException
	 *             falls die Anzahl der Werte nicht mit der der Richtungen
	 *             uebereinstimmt
	 */
	public TuringTransitionOutput(MultiTapeReadWriteData<T> toWrite,
			Direction... directions) {

		if (toWrite.getLength() != directions.length) {
			throw new IllegalArgumentException(
					"Die Anzahl der Werte stimmt nicht mit der der Richtungen überein");
		}
		this.toWrite = toWrite;
		this.directions = directions;
	}
	/**
	 * Liefert die zu schreibenden Daten.
	 * 
	 * @return Die zu schreibenden Daten
	 */
	public MultiTapeReadWriteData<T> getToWrite() {
		return toWrite;
	}
	/**
	 * Liefert die Bewegungsrichtungen der einzelnen Koepfe.
	 * 
	 * @return Bewegungsrichtungen der einzelnen Koepfe
	 */
	public Direction[] getDirections() {
		return directions;
	}
}
