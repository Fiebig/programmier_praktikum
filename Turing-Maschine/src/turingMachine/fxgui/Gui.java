package turingMachine.fxgui;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import turingMachine.TuringMachine;
import turingMachine.io.TuringMachineReader;

/**
 * Erstellt eine graphische Benutzeroberfläche, die den Ablauf einer
 * Turing-Berechnung darstellt.
 * 
 * @author Alexander Fiebig
 * 
 */
public class Gui extends Application {
	/**
	 * Die aktuell geladene Maschine.
	 */
	private TuringMachine<Character> currentMachine;

	/**
	 * Startet die GUI.
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(final Stage primaryStage) {
		primaryStage.setTitle("Turing-Maschine");
		/*
		 * TextArea fuer das Oeffnen von Textdateien mit
		 * Turing-Maschinen-Definition
		 */
		final TextArea machineText = new TextArea("");
		// machineText.setMaxSize(350, 650);
		machineText.setEditable(true);
		machineText.setPromptText("MachineText");
		// TextArea fuer die Ausgabe von Statusmeldungen
		final TextArea statusText = new TextArea();
		statusText.setPromptText("StatusText");
		// statusText.setMaxSize(850, 150);
		statusText.setEditable(false);
		// Checkbox, die anzeigt ob Maschine in einem aktzeptierten Zustand ist
		final CheckBox cb = new CheckBox("InAcceptedState");
		cb.setIndeterminate(true);
		// Label fuer currentStateField
		final Text txt = new Text("Current State:");
		// TextField, das den aktuellen Zustand der Maschine anzeigt
		final TextField currentStateField = new TextField();
		currentStateField.setText("Noch keine Maschine geladen");
		currentStateField.setMinSize(220, 20);
		currentStateField.setEditable(false);
		// Hilfsklasse zum Anzeigen der einzelnen Baender
		final MultiTapeControl<Character> view = new MultiTapeControl<Character>();
		// Button fuer das Einlesen von Dateien in machineText
		final Button openButton = new Button("Open");

		openButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				try {
					Stage stage = new Stage();
					FileChooser fileChooser = new FileChooser();
					File file = fileChooser.showOpenDialog(stage);
					if (file != null) {
						InputStream in = new FileInputStream(file);
						InputStreamReader reader = new InputStreamReader(in);
						BufferedReader data = new BufferedReader(reader);
						String textForMachine = "";
						String aktuelleZeile;
						while ((aktuelleZeile = data.readLine()) != null) {
							textForMachine = textForMachine + aktuelleZeile + "\n";
						}
						data.close();
						machineText.setText(textForMachine);
						statusText.appendText("MaschinenDatei wurde geoeffnet\n");
					}
				}
				catch (IOException ex) {
					statusText.appendText("ERROR: I/O-Fehler beim oeffnen der MaschinenDatei\n");
				}
			}
		});

		/*
		 * Button, der einzelne Transitionen auf der geladenen Maschine
		 * ausfuehrt
		 */
		final Button stepBtn = new Button();
		stepBtn.setText("Step");
		stepBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (currentMachine != null) {
					try {
						String startState = currentMachine.getCurrentState().toString();
						currentMachine.transit();
						String targetState = currentMachine.getCurrentState().toString();
						statusText.appendText(startState + " -> " + targetState + "\n");
						currentStateField.setText(targetState);
						if (currentMachine.isInAcceptedState()) {
							statusText.appendText("Accepted state reached\n");
							cb.setSelected(true);
						}
					}
					catch (IllegalArgumentException | IllegalStateException e) {
						statusText.appendText("ERROR: " + e.getMessage() + "\n");
					}
				}
				else {
					statusText.appendText("ERROR: Es wurde noch keine Maschine geladen\n");
				}
			}
		});
		/*
		 * Timeline, die nach einer festgelegten Zeitspanne wiederholt
		 * Transitionen auf der Maschine ausfuehrt, bis diese einen akzeptierten
		 * Zustand erreicht
		 */
		final Timeline timeline = new Timeline();
		timeline.setCycleCount(Animation.INDEFINITE);
		/*
		 * KeyFrame fuer timeline. Zeitspanne wird zunaechst auf 1000ms
		 * festgelegt
		 */
		final KeyFrame transit = new KeyFrame(Duration.millis(1000), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				try {
					String startState = currentMachine.getCurrentState().toString();
					currentMachine.transit();
					String targetState = currentMachine.getCurrentState().toString();
					statusText.appendText(startState + " -> " + targetState + "\n");
					currentStateField.setText(targetState);
					if (currentMachine.isInAcceptedState()) {
						statusText.appendText("Accepted state reached\n");
						cb.setSelected(true);
						timeline.stop();
					}
				}
				catch (IllegalArgumentException | IllegalStateException e) {
					statusText.appendText("ERROR: " + e.getMessage() + "\n");
				}
			}
		});

		timeline.getKeyFrames().add(transit);
		// Label fuer timeSlider
		final Text txt3 = new Text("Delay:");
		txt3.setFont(Font.font("Arial", FontWeight.BOLD, 16));
		/*
		 * Slider mit dem sich die Zeitspanne zwischen den einzelnen
		 * Transitionen im Bereich von 20ms bis 2000ms einstellen laesst
		 */
		final Slider timeSlider = new Slider();
		timeSlider.setMaxSize(250, 30);
		timeSlider.setMinSize(250, 30);
		timeSlider.setMin(20);
		timeSlider.setMax(2000);
		timeSlider.setValue(1000);
		timeSlider.setShowTickLabels(true);
		timeSlider.setMajorTickUnit(1980);

		timeSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				KeyFrame newTransit = new KeyFrame(Duration.millis(new_val.intValue()),
						new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						try {
							String startState = currentMachine.getCurrentState().toString();
							currentMachine.transit();
							String targetState = currentMachine.getCurrentState().toString();
							statusText.appendText(startState + " -> " + targetState + "\n");
							currentStateField.setText(targetState);
							if (currentMachine.isInAcceptedState()) {
								statusText.appendText("Accepted state reached\n");
								cb.setSelected(true);
								timeline.stop();
							}
						}
						catch (IllegalArgumentException | IllegalStateException e) {
							statusText.appendText("ERROR: " + e.getMessage() + "\n");
						}
					}
				});
				boolean isRunning = ((timeline.getStatus() == Status.RUNNING) ? true : false);
				timeline.stop();
				timeline.getKeyFrames().setAll(newTransit);
				if (isRunning == true) {
					timeline.play();
				}

			}
		});
		/*
		 * Button, der eine Turing-Maschine aus dem Inhalt von machineText
		 * erstellt und diese auf currentMachine speichert
		 */
		final Button loadBtn = new Button();
		loadBtn.setText("Load");
		loadBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					timeline.stop();
					InputStream in = new ByteArrayInputStream(machineText.getText().getBytes());
					currentMachine = TuringMachineReader.read(in);
					view.setTapes(currentMachine.getTapes());
					statusText.appendText("Machine loaded...\n");
					cb.setIndeterminate(false);
					cb.setSelected(currentMachine.isInAcceptedState());
					currentStateField.setText(currentMachine.getCurrentState().toString());
				}
				catch (IOException ex) {
					statusText.appendText("ERROR: Ungueltige MaschinenDatei: " + ex.getMessage() + "\n");
				}
			}
		});
		// Button, der die Timeline startet
		final Button startBtn = new Button();
		startBtn.setText("Start");
		startBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (currentMachine != null) {
					if (!currentMachine.isInAcceptedState()) {
						timeline.play();
					}
					else {
						statusText.appendText(
								"ERROR: Die Maschine befindet sich bereits in einem aktzeptierten Zustand\n");
					}
				}
				else {
					statusText.appendText("ERROR: Es wurde noch keine Maschine geladen\n");
				}
			}
		});
		// Button, der die Timeline stoppt
		final Button stopBtn = new Button();
		stopBtn.setText("Stop");
		stopBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				timeline.stop();
			}
		});
		// HBox mit allen Buttons und dem Slider
		final HBox hbButtons = new HBox();
		hbButtons.setSpacing(20);
		hbButtons.setPadding(new Insets(10, 20, 10, 20));
		// Auto-sizing spacer
		Region spacer = new Region();
		hbButtons.getChildren().addAll(stepBtn, startBtn, stopBtn, txt3, timeSlider, spacer, openButton, loadBtn);
		hbButtons.setStyle("-fx-background-color: #336699;");
		HBox.setHgrow(spacer, Priority.ALWAYS);
		// HBox.setMargin(loadBtn, new Insets(0, 40, 0, 0));
		// HBox mit der Checkbox und dem currentStateField
		final HBox stateInfo = new HBox();
		stateInfo.setSpacing(20);
		stateInfo.getChildren().addAll(txt, currentStateField, cb);
		// VBox mit hbButtons und view
		final VBox center = new VBox();
		center.getChildren().addAll(stateInfo, view);
		VBox.setMargin(view, new Insets(30, 0, 0, 0));
		// Hauptebene
		final BorderPane main = new BorderPane();
		main.setRight(machineText);
		main.setTop(hbButtons);
		main.setBottom(statusText);
		main.setCenter(center);
		BorderPane.setMargin(center, new Insets(20, 0, 0, 20));

		primaryStage.setScene(new Scene(main, 1200, 700));
		primaryStage.show();
	}

}
