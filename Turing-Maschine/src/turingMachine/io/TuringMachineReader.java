package turingMachine.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import turingMachine.TuringMachine;
import turingMachine.TuringTransitionOutput;
import turingMachine.tape.Direction;
import turingMachine.tape.MultiTapeReadWriteData;
import turingMachine.tape.Tape;

/**
 * Kann eine Definition einer Turing-Maschine mit einem Character-Alphabet von
 * einem Eingabestrom einlesen und daraus ein TuringMachine-Objekt erstellen.
 * Die Definition einer Maschine sieht folgendermassen aus:<br>
 * <br>
 * 
 * [Anzahl der Baender]<br>
 * [Vorinitialisierung Band 1]<br>
 * [Vorinitialisierung Band 2]<br>
 * …<br>
 * [Komma-separierte Zustandsliste]<br>
 * [Startzustand]<br>
 * [Komma-separierte Liste der akzeptierten Zustaende]<br>
 * [Transition 1]<br>
 * [Transition 2]<br>
 * …<br>
 * <br>
 * Hierbei haben die Vorinitialisierungen für ein Band das Format<br>
 * 
 * [Position des Lesekopfes (startend bei 1)]:[Bandinhalt ab Position 1]<br>
 * <br>
 * 
 * und eine Transition hat das Format<br>
 * 
 * [Name des Anfangszustands],[Inhalt der einzelnen Baender] -> [Name des
 * Zielzustands],[Werte, die auf die einzelnen Baender zu schreiben
 * sind],[Richtungen für die einzelnen Koepfe: R für Rechts; L für Links, N für
 * keine Bewegung]<br>
 * <br>
 * 
 * Zeilen, die mit einer Raute beginnen oder komplett leere Zeilen werden
 * ignoriert. Unterstriche („_“) stehen für leere Speicherzellen.
 * 
 * 
 * @author Alexander Fiebig
 * 
 */
public class TuringMachineReader {

	/**
	 * Liest eine Turing-Maschine aus einem InputStream und erstellt dazu ein
	 * {@code TuringMachine<Character>}-Objekt.
	 * 
	 * @param input
	 *            InputStream von dem gelesen wird
	 * @return Entsprechendes {@code TuringMachine<Character>}-Objekt
	 * @throws IOException
	 *             falls die Eingabe ungueltig ist
	 */

	public static TuringMachine<Character> read(InputStream input)
			throws IOException {
		InputStreamReader reader = new InputStreamReader(input);
		return read(reader);
	}

	/**
	 * Liest eine Turing-Maschine aus einem Reader und erstellt dazu ein
	 * {@code TuringMachine<Character>}-Objekt.
	 * 
	 * @param input
	 *            Reader von dem gelesen wird
	 * @return Entsprechendes {@code TuringMachine<Character>}-Objekt
	 * @throws IOException
	 *             falls die Eingabe ungueltig ist
	 */
	public static TuringMachine<Character> read(Reader input)
			throws IOException {
		try (BufferedReader data = new BufferedReader(input)) {
			String aktuelleZeile;
			if ((aktuelleZeile = getNextImportantLine(data)) == null) {
				throw new IOException("Unvollstaendige Eingabe");
			}
			// Anzahl der Baender einlesen und entsprechende Maschine erstellen
			int anzahlBaender = Integer.parseInt(aktuelleZeile);
			TuringMachine<Character> tM = new TuringMachine<Character>(
					anzahlBaender);
			readTapeInitialisations(tM, data);
			readStates(tM, data);
			readTransitions(tM, data);
			return tM;
		}
		catch (IllegalArgumentException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	/**
	 * Liest Vorinitialisierung der Baender ein.
	 * 
	 * @param tM
	 *            Zu erstellende Turing-Maschine
	 * @param data
	 *            BufferedReader der die Daten bereitstellt
	 * @throws IOException
	 *             falls ein I/O-Fehler auftritt
	 */
	private static void readTapeInitialisations(
			final TuringMachine<Character> tM, final BufferedReader data)
			throws IOException {
		String aktuelleZeile;
		String[] split;
		try {
			for (Tape<Character> currentTape : tM.getTapes().getTapes()) {
				if ((aktuelleZeile = getNextImportantLine(data)) == null) {
					throw new IOException("Unvollstaendige Eingabe");
				}
				split = aktuelleZeile.split("\\s*:\\s*");
				if (split.length != 2) {
					throw new IOException(
							"Ungueltige Vorinitialisierung der Baender: Falsche Zeilenform");
				}
				// Bandinhalt setzen
				for (int j = 0; j < split[1].length(); j++) {
					char content = split[1].charAt(j);
					if (Character.isWhitespace(content)) {
						throw new IOException(
								"Ungueltige Vorinitialisierung der Baender: Kein Whitespace im Bandinhalt erlaubt");
					}
					if (content != '_') {
						currentTape.write(content);
					}
					currentTape.move(Direction.RIGHT);
				}
				// Position des Kopfes setzen
				int headPosition = Integer.parseInt(split[0]);
				currentTape.setHeadPosition(headPosition - 1);
			}
		}
		catch (IllegalArgumentException e) {
			throw new IOException("Ungueltige Vorinitialisierung der Baender: "
					+ e.getMessage(), e);
		}
	}

	/**
	 * Liest die Zustandsinitialisierung ein.
	 * 
	 * @param tM
	 *            Zu erstellende Turing-Maschine
	 * 
	 * @param data
	 *            BufferedReader der die Daten bereitstellt
	 * @throws IOException
	 *             falls ein I/O-Fehler auftritt
	 */
	private static void readStates(final TuringMachine<Character> tM,
			final BufferedReader data) throws IOException {
		String aktuelleZeile;
		String[] split;
		try {
			if ((aktuelleZeile = getNextImportantLine(data)) == null) {
				throw new IOException("Unvollstaendige Eingabe");
			}
			// Zustaende einlesen
			split = aktuelleZeile.split(",");
			for (int i = 0; i < split.length; i++) {
				tM.addState(split[i]);
			}
			if ((aktuelleZeile = getNextImportantLine(data)) == null) {
				throw new IOException("Unvollstaendige Eingabe");
			}
			// Startzustand einlesen
			tM.setCurrentState(aktuelleZeile);
			if ((aktuelleZeile = getNextImportantLine(data)) == null) {
				throw new IOException("Unvollstaendige Eingabe");
			}
			// Aktzeptierte Zustaende einlesen
			split = aktuelleZeile.split(",");
			for (int i = 0; i < split.length; i++) {
				tM.getState(split[i]).setAccepted(true);
			}
		}
		catch (IllegalStateException e) {
			throw new IOException("Ungueltige Zustandsangabe: "
					+ e.getMessage(), e);
		}
	}

	/**
	 * Liest die Uebergaenge ein.
	 * 
	 * * @param tM Zu erstellende Turing-Maschine
	 * 
	 * @param data
	 *            BufferedReader der die Daten bereitstellt
	 * @throws IOException
	 *             falls ein I/O-Fehler auftritt
	 */
	private static void readTransitions(final TuringMachine<Character> tM,
			final BufferedReader data) throws IOException {
		String aktuelleZeile;
		String[] split;
		try {
			while ((aktuelleZeile = getNextImportantLine(data)) != null) {
				split = aktuelleZeile.split("\\s*->\\s*");
				if (split.length != 2) {
					throw new IOException(
							"Ungueltige Uebergangsangabe: Falsche Zeilenform");
				}
				// Enthaelt Startzustand und Intput-Informationen
				String[] subSplit1 = split[0].split(",");
				// Enthaelt Zielzustand und Output-Informationen
				String[] subSplit2 = split[1].split(",");
				// Pruefen ob Zeilenform stimmt
				if (subSplit1.length != 2 || subSplit2.length != 3
						|| subSplit1[1].length() != tM.getTapeCount()
						|| subSplit2[1].length() != tM.getTapeCount()
						|| subSplit2[2].length() != tM.getTapeCount()) {
					throw new IOException(
							"Ungueltige Uebergangsangabe: Falsche Zeilenform");
				}
				// Uebergang erstellen
				tM.addTransition(subSplit1[0],
						createTransitionInput(subSplit1[1]), subSplit2[0],
						createTransitionOutput(subSplit2[1], subSplit2[2]));
			}
		}
		catch (IllegalStateException e) {
			throw new IOException("Ungueltige Uebergangsangabe: "
					+ e.getMessage(), e);
		}
	}

	/**
	 * Erzeugt das Input-Objekt fuer einen Uebergang.
	 * 
	 * @param tapeContent
	 *            Baenderinhalt vor einem Uebergang als String
	 * @throws IOException
	 *             falls Daten auf tapeContent ungueltig sind
	 * 
	 * @return Input-Objekt fuer den entsprechenden Uebergang
	 */
	private static MultiTapeReadWriteData<Character> createTransitionInput(
			final String tapeContent) throws IOException {
		MultiTapeReadWriteData<Character> startData = new MultiTapeReadWriteData<Character>(
				tapeContent.length());
		for (int i = 0; i < tapeContent.length(); i++) {
			char cellContent = tapeContent.charAt(i);
			if (Character.isWhitespace(cellContent)) {
				throw new IOException(
						"Ungueltige Vorinitialisierung der Baender: Kein Whitespace im Bandinhalt erlaubt");
			}
			if (cellContent != '_') {
				startData.set(i, cellContent);
			}
		}
		return startData;
	}

	/**
	 * Erzeugt das Output-Objekt fuer einen Uebergang.
	 * 
	 * @param tapeContent
	 *            Baenderinhalt nach einem Uebergang als String
	 * @param directionsAsString
	 *            Bewegungsrichtungen der einzelnen Lese-/Schreibkoepfe als
	 *            String
	 * @throws IOException
	 *             falls Daten auf tapeContent oder directionsAsString ungueltig
	 *             sind
	 * @return Output-Objekt fuer den entsprechenden Uebergang
	 */
	private static TuringTransitionOutput<Character> createTransitionOutput(
			final String tapeContent, final String directionsAsString)
			throws IOException {
		// Inhalt fuer Output-Objekt
		MultiTapeReadWriteData<Character> dataForOutput = new MultiTapeReadWriteData<Character>(
				tapeContent.length());
		for (int i = 0; i < tapeContent.length(); i++) {
			char cellContent = tapeContent.charAt(i);
			if (Character.isWhitespace(cellContent)) {
				throw new IOException(
						"Ungueltige Vorinitialisierung der Baender: Kein Whitespace im Bandinhalt erlaubt");
			}
			if (cellContent != '_') {
				dataForOutput.set(i, cellContent);
			}
		}
		// Richtungen fuer Output-Objekt
		Direction[] directions = new Direction[directionsAsString.length()];
		for (int i = 0; i < directionsAsString.length(); i++) {
			char content = directionsAsString.charAt(i);
			if (content == 'R') {
				directions[i] = Direction.RIGHT;
			}
			else if (content == 'L') {
				directions[i] = Direction.LEFT;
			}
			else if (content == 'N') {
				directions[i] = Direction.NON;
			}
			else {
				throw new IOException(
						"Ungueltige Vorinitialisierung der Baender: Ungültige Direction");
			}
		}
		// Output-Objekt erstellen
		TuringTransitionOutput<Character> output = new TuringTransitionOutput<Character>(
				dataForOutput, directions);
		return output;
	}

	/**
	 * Liefert die naechste Zeile von data, die nicht zu ignorieren ist. Zu
	 * ignorierende Zeilen sind leere Zeilen oder Zeilen, die mit einer Raute
	 * beginnen.
	 * 
	 * @param data
	 *            BufferedReader der die Daten bereitstellt
	 * 
	 * @return String mit der naechsten Zeile von data, die nicht zu ignorieren
	 *         ist oder null falls das Ende von data erreicht wird.
	 * @throws IOException
	 *             falls ein I/O-Fehler auftritt
	 */
	private static String getNextImportantLine(final BufferedReader data)
			throws IOException {
		String aktuelleZeile;
		do {
			aktuelleZeile = data.readLine();
			if (aktuelleZeile == null) {
				return null;
			}
			aktuelleZeile = aktuelleZeile.trim();
		}
		while (aktuelleZeile.isEmpty() || aktuelleZeile.charAt(0) == '#');
		return aktuelleZeile;
	}

	/**
	 * Beispiel:<br>
	 * Es wird eine Turing-Maschine eingelesen, die die Einsen auf dem Band
	 * verdoppelt. Anschliessend wird die Maschine gestartet und das Ergebnis
	 * ausgegeben.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String example = "# Turingmaschinendefinition fuer Character-Baender. Leerzeilen und Zeilen mit\n"
				+ "# beginnend werden ignoriert\n"
				+ "# Diese Machine verdoppelt die Einsen auf dem Band\n"
				+ "# Anzahl der Baender\n"
				+ "\n"
				+ "1\n"
				+ "# Vorinitalsierung der Baender\n"
				+ "1 : 11111\n"
				+ "# Zustaende der Maschine\n"
				+ "s1,s2,s3,s4,s5,s6\n"
				+ "# Startzustand\n"
				+ "s1\n"
				+ "# akzeptierte Zustaende\n"
				+ "s6\n"
				+ "# Tranistionen\n"
				+ "s1,1 -> s2,_,R\n"
				+ "s1,_ -> s6,_,N\n"
				+ "s2,1 -> s2,1,R\n"
				+ "s2,_ -> s3,_,R\n"
				+ "s3,_ -> s4,1,L\n"
				+ "s3,1 -> s3,1,R\n"
				+ "s4,1 -> s4,1,L\n"
				+ "s4,_ -> s5,_,L\n"
				+ "s5,1 -> s5,1,L\n"
				+ "s5,_ -> s1,1,R\n"
				+ "s6,_ -> s6,_,N";

		InputStream in = new ByteArrayInputStream(example.getBytes());
		TuringMachine<Character> exampleMachine = read(in);
		exampleMachine.run();
		System.out.println(exampleMachine.getTapes().toString());
	}

}
