package turingMachine.tape;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Stellt ein einzelnes Band dar.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der zugehoerigen Turing-Maschine
 */
public class Tape<T> {
	/**
	 * Speichert die TapeChangeListener, die ueber Aenderungen am Band
	 * informiert werden muessen.
	 */
	private final List<TapeChangeListener<T>> listeners;
	/**
	 * Speichert die Daten des Bandes.
	 */
	private final List<T> tapeData;
	/**
	 * Gibt die momentane Position des Lese-/Schreibkopfes an.
	 */
	private int currentHeadPosition;
	/**
	 * Stellt den Lese-/Schreibkopf dar.
	 */
	private T head;

	/**
	 * Erstellt ein neues Band mit einer leeren Zelle.
	 */
	public Tape() {
		listeners = new LinkedList<TapeChangeListener<T>>();
		tapeData = new ArrayList<T>();
		currentHeadPosition = 0;
		tapeData.add(null);
		head = null;
	}

	/**
	 * Setzt die Position des Lese-/Schreibkopfes auf position (nur fuer
	 * TuringMachineReader). Dabei wird das Band nicht bewegt/erweitert.
	 * 
	 * @param position
	 *            Neue Position des Lese-/Schreibkopfes
	 * 
	 * @throws IllegalArgumentException
	 *             falls die Zelle mit der Position position nicht existiert
	 */
	public void setHeadPosition(int position) {
		if (position < 0 || position >= tapeData.size()) {
			throw new IllegalArgumentException("Ungueltige Kopfposition");
		}
		currentHeadPosition = position;
		head = tapeData.get(currentHeadPosition);
	}

	/**
	 * Bewegt den Kopf in die Richtung direction.
	 * 
	 * @param direction
	 *            Gewuenschte Richtung
	 */
	public void move(Direction direction) {

		if (direction == Direction.LEFT) {
			currentHeadPosition--;
			if (currentHeadPosition < 0) {
				tapeData.add(0, null);
				head = null;
				currentHeadPosition = 0;
				// Informierung der Listener ueber Expandierung
				for (TapeChangeListener<T> tpl : listeners) {
					tpl.onExpand(direction);
				}
			}
			else {
				head = tapeData.get(currentHeadPosition);
			}

		}
		else if (direction == Direction.RIGHT) {
			currentHeadPosition++;
			if (currentHeadPosition >= tapeData.size()) {
				tapeData.add(null);
				head = null;
				currentHeadPosition = tapeData.size() - 1;
				// Informierung der Listener ueber Expandierung
				for (TapeChangeListener<T> tpl : listeners) {
					tpl.onExpand(direction);
				}
			}
			else {
				head = tapeData.get(currentHeadPosition);
			}

		}
		else {
			head = tapeData.get(currentHeadPosition);
		}
		// Informierung der Listener ueber Bewegung
		for (TapeChangeListener<T> tpl : listeners) {
			tpl.onMove(direction);
		}

	}

	/**
	 * Gibt das aktuelle Element unter dem Kopf zurück. Steht der Kopf über
	 * einer leeren Stelle, wird null zurueckgegeben.
	 * 
	 * @return Aktuelles Element unter dem Kopf
	 */
	public T read() {
		return head;
	}

	/**
	 * Gibt den Inhalt des Bandes inklusive leerer Zellen an Anfang und Ende
	 * (alle Zellen auf denen der Kopf schon einmal war) zurueck.
	 * 
	 * @return Liste des Bandinhalts, die nicht mehr modifizierbar ist.
	 */
	public List<T> getContents() {
		return Collections.unmodifiableList(tapeData);
	}

	/**
	 * Gibt die Position des Kopfes auf dem Band zurueck. Entspricht der
	 * Position in der von getContents() zurueckgegebenen Liste.
	 * 
	 * @return Position des Kopfes
	 */
	public int getPosition() {
		return currentHeadPosition;
	}

	/**
	 * Schreibt den Inhalt von content an die aktuelle Stelle des Kopfes.
	 * 
	 * @param content
	 *            Zu schreibender Inhalt
	 */
	public void write(T content) {
		tapeData.set(currentHeadPosition, content);
		head = tapeData.get(currentHeadPosition);
		// Informierung der Listener ueber Schreibaktion
		for (TapeChangeListener<T> tpl : listeners) {
			tpl.onWrite(content);
		}

	}

	/**
	 * Fuegt einen neuen TapeChangeListener zu den, zu informierenden, Listenern
	 * hinzu. Auf diese Art hinzugefuegte Listener werden ueber Bandoperationen
	 * entsprechend der Interfacevereinbarung informiert.
	 * 
	 * @param listener
	 *            Neuer TapeChangeListener
	 */
	public void addListener(TapeChangeListener<T> listener) {
		if (listeners.contains(listener) == false) {
			listeners.add(listener);
		}
	}

	/**
	 * Gibt das aktuelle Element unter dem Kopf als String zurueck
	 * (toString()-Methoden des Speicherzelleninhalts). Befindet sich unter dem
	 * Kopf eine leere Zelle, wird ein Unterstrich „_“ zurueckgegeben.
	 * 
	 * @return Aktuelles Element unter dem Kopf als String
	 */
	public String getCurrent() {
		if (head == null) {
			return "_";
		}
		return head.toString();
	}

	/**
	 * Gibt den Bandinhalt links von der aktuellen Position als String
	 * (verknpuefte toString()-Methoden der Speicherzelleninhalte) zurueck.
	 * Fuehrende, leere Zellen werden abgeschnitten, allerdings maximal bis zu
	 * der Zelle, auf der aktuell der Kopf steht. Ist der linke Teil des Bandes
	 * leer, wird ein leerer String „“ zurueckgegeben.
	 * 
	 * @return Bandinhalt links von der aktuellen Position als String
	 */
	public String getLeft() {
		StringBuilder sb = new StringBuilder(tapeData.size());
		for (ListIterator<T> i = tapeData.listIterator(0); i.hasNext();) {
			if (i.nextIndex() == currentHeadPosition) {
				break;
			}
			head = i.next();
			if (head != null) {
				sb.append(getCurrent());
			}
		}
		head = tapeData.get(currentHeadPosition);
		return sb.toString();
	}

	/**
	 * Gibt den Bandinhalt rechts von der aktuellen Position als String
	 * (verknpuefte toString()-Methoden der Speicherzelleninhalte) zurueck.
	 * Abschliessende, leere Zellen werden abgeschnitten, allerdings maximal bis
	 * zu der Zelle, auf der aktuell der Kopf steht. Ist der rechte Teil des
	 * Bandes leer, wird ein leerer String „“ zurueckgegeben.
	 * 
	 * @return Bandinhalt rechts von der aktuellen Position als String
	 */
	public String getRight() {
		StringBuilder sb = new StringBuilder(tapeData.size());
		for (ListIterator<T> i = tapeData.listIterator(tapeData.size()); i
				.hasPrevious();) {
			if (i.previousIndex() == currentHeadPosition) {
				break;
			}
			head = i.previous();
			if (head != null) {
				sb.insert(0, getCurrent());
			}
		}
		head = tapeData.get(currentHeadPosition);
		return sb.toString();
	}

	/**
	 * Gibt den Inhalt des kompletten Bandes als zweizeiligen String an. Die
	 * erste Zeile ist der Inhalt des Bandes (verknuefte toString()-Methoden der
	 * Speicherzelleninhalte) ohne fuehrende und abschliessende Leerzellen
	 * (maximal wird bis zu der Zelle abgeschnitten, auf der gerade der Kopf
	 * steht). Die Zweite Zeile besteht aus Leerzeichen, lediglich unter der
	 * Zelle, auf der sich der Kopf befindet, steht das Zeichen „^“.
	 * 
	 * @return Inhalt des kompletten Bandes inklusive Kopfposition als
	 *         zweizeiliger String
	 */
	@Override
	public String toString() {
		String temp = getLeft() + getCurrent() + getRight() + "\n";
		for (int i = 0; i < getLeft().length(); i++) {
			temp = temp + " ";
		}
		temp = temp + "^";
		for (int i = 0; i < getRight().length(); i++) {
			temp = temp + " ";
		}
		return temp;
	}

}
