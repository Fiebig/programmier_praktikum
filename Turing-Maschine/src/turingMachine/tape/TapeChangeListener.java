package turingMachine.tape;
/**
 * Stellt das Geruest fuer einen Listener dar, der die Aenderung von Tape
 * Objekten verfolgt.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der zugehoerigen Turing-Maschine
 */
public interface TapeChangeListener<T> {
	/**
	 * Wird aufgerufen, nachdem ein Band bewegt wurde (auch bei Direction.NON).
	 * 
	 * @param direction
	 *            Richtung, in die das Band bewegt wurde
	 */
	public void onMove(Direction direction);
	/**
	 * Wird aufgerufen, nachdem eine Zelle zum ersten mal vom Lesekopf besucht
	 * wurde. Tritt dies in Kombination mit einer Bandbewegung auf, wird onMove
	 * nach dieser Methode aufgerufen.
	 * 
	 * @param direction
	 *            Richtung, in die das Band bewegt wurde
	 */
	public void onExpand(Direction direction);
	/**
	 * Wird aufgerufen, wann immer auf ein Band geschrieben wurde.
	 * 
	 * @param value
	 *            Wert, der auf das Band geschrieben wurde
	 */
	public void onWrite(T value);
}
