package turingMachine.tape;
/**
 * Stellt 3 Konstanten zur Verfuegung, die die Moeglichkeiten zur
 * Bewegungsrichtung der Schreib-/Lesekoepfe darstellen.
 * 
 * @author Alexander Fiebig
 * 
 */
public enum Direction {
	// Moegliche Bewegungsrichtungen
	LEFT, RIGHT, NON
}
