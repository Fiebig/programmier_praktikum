package turingMachine.tape;

import java.util.HashMap;
/**
 * Repraesentiert die Daten, die von einem MultiTape (genauer: den Koepfen der
 * einzelnen Baender) gelesen wurden, oder auf dieses geschrieben werden sollen.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der zugehoerigen Turing-Maschine
 */
public class MultiTapeReadWriteData<T> {
	/**
	 * Speichert die Daten unter den Koepfen der einzelnen Baender.
	 */
	private final HashMap<Integer, T> data;
	/**
	 * Erstellt ein MultiTapeReadWriteData-Objekt mit length Werten.
	 * 
	 * @param length
	 *            Anzahl der Werte
	 */
	public MultiTapeReadWriteData(int length) {
		data = new HashMap<Integer, T>(length);
		for (int i = 0; i < length; i++) {
			data.put(i, null);
		}
	}
	/**
	 * Liefert den Wert, der fuer den Inhalt der Zelle unter dem
	 * Lese-/Schreibkopf des i-ten Bandes steht. Die Indizierung erfolgt
	 * 0-basiert.
	 * 
	 * @param i
	 *            Bandnummer, beginnend mit 0
	 * @return Inhalt der Zelle unter dem Lese-/Schreibkopf des i-ten Bandes
	 * 
	 * @throws IndexOutOfBoundsException
	 *             falls versucht wird auf eine Zelle mit einem Index außerhalb
	 *             des zulaessigen Bereiches zuzugreifen
	 */
	public T get(int i) {
		if (i < 0 || i >= getLength()) {
			throw new IndexOutOfBoundsException(
					"Parameter i muss eine Zahl zwischen 0 und "
							+ (getLength() - 1) + " sein");
		}
		return data.get(i);
	}
	/**
	 * Setzt den Wert, der für den Inhalt der Zelle unter dem Lese-/Schreibkopf
	 * des i-ten Bandes steht. Die Indizierung erfolgt 0-basiert.
	 * 
	 * @param i
	 *            Bandnummer, beginnend mit 0
	 * @param value
	 *            Inhalt der Zelle unter dem Lese-/Schreibkopf des i-ten Bandes
	 * 
	 * @throws IndexOutOfBoundsException
	 *             falls versucht wird auf eine Zelle mit einem Index außerhalb
	 *             des zulaessigen Bereiches zuzugreifen
	 */
	public void set(int i, T value) {
		if (i < 0 || i >= getLength()) {
			throw new IndexOutOfBoundsException(
					"Parameter i muss eine Zahl zwischen 0 und "
							+ (getLength() - 1) + " sein");
		}
		data.put(i, value);
	}
	/**
	 * Liefert die Anzahl der Werte (entspricht der Anzahl der repraesentierten
	 * Baender-Koepfe).
	 * 
	 * @return Anzahl der Werte
	 */
	public int getLength() {
		return data.size();
	}
	/**
	 * Vergleicht zwei MultiTapeReadWriteData-Objekte anhand ihrer
	 * toString()-Methoden (sind genau dann gleich, wenn die Werte auf den
	 * einzelnen Baendern paarweise gleich sind).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object other) {

		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (other.getClass() != this.getClass()) {
			return false;
		}
		if (!(this.toString().equals(((MultiTapeReadWriteData<T>) other)
				.toString()))) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int hc = 23;// willkürlicher Anfangswert
		int hashMultiplier = 59;// kleine Primzahl
		byte[] temp = this.toString().getBytes();
		int fin = 0;
		for (int i = 0; i < temp.length; i++) {
			fin += temp[i];
		}
		hc = hc * hashMultiplier + fin;
		return hc;
	}
	/**
	 * Gibt die einzelnen Werte als String (verknuefte toString()-Methoden)
	 * zurueck. Null-Werte werden hierbei als Unterstrich („_“) dargestellt.
	 * 
	 * @return Werte als String
	 */
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < getLength(); i++) {
			if (get(i) == null) {
				s = s + "_";
			} else {
				s = s + get(i).toString();
			}
		}
		return s;
	}

}
