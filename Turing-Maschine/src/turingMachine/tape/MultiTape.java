package turingMachine.tape;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Stellt eine Verbindung aus mehreren einzelnen Baendern dar.
 * 
 * @author Alexander Fiebig
 * 
 * @param <T>
 *            Eingabe- und Ausgabealphabet der zugehoerigen Turing-Maschine
 */
public class MultiTape<T> {
	/**
	 * Speichert die einzelnen Baender
	 */
	private final List<Tape<T>> tapes;

	/**
	 * Erstellt ein neues MultiTape mit tapecount einzelnen Baendern
	 * 
	 * @param tapeCount
	 *            Anzahl der Baender
	 */
	public MultiTape(int tapeCount) {
		tapes = new ArrayList<Tape<T>>(tapeCount);
		for (int i = 0; i < tapeCount; i++) {
			Tape<T> t = new Tape<T>();
			tapes.add(t);
		}
	}

	/**
	 * Liefert eine Liste der Baender.
	 * 
	 * @return Liste der Baender
	 */
	public List<Tape<T>> getTapes() {
		return tapes;
	}

	/**
	 * Gibt die Anzahl der Baender zurueck.
	 * 
	 * @return Anzahl der Baender
	 */
	public int getTapeCount() {
		return tapes.size();
	}

	/**
	 * Liest die Werte unter allen Koepfen der einzelnen Baender und gibt diese
	 * zurueck.
	 * 
	 * @return Werte unter allen Koepfen der einzelnen Baender
	 */
	public MultiTapeReadWriteData<T> read() {
		MultiTapeReadWriteData<T> mtrwd = new MultiTapeReadWriteData<T>(
				getTapeCount());
		int temp = 0;
		for (ListIterator<Tape<T>> i = tapes.listIterator(0); i.hasNext();) {
			mtrwd.set(temp, i.next().read());
			temp++;

		}
		return mtrwd;
	}

	/**
	 * Schreibt die Werte in values auf die einzelnen Baender.
	 * 
	 * @param values
	 *            MultiTapeReadWriteData-Objekt auf dem die Werte stehen
	 * 
	 * @throws IllegalArgumentException
	 *             falls sich zu wenige oder zu viele Werte in values befinden
	 */
	public void write(MultiTapeReadWriteData<T> values) {
		if (values.getLength() != getTapeCount()) {
			throw new IllegalArgumentException(
					"Es befinden sich in values zu wenige oder zu viele Werte");
		}
		int temp = 0;
		for (ListIterator<Tape<T>> i = tapes.listIterator(0); i.hasNext();) {
			i.next().write(values.get(temp));
			temp++;
		}
	}

	/**
	 * Bewegt die Lese- und Schreibkoepfe der einzelnen Baender.
	 * 
	 * @param directions
	 *            Array mit den einzelnen Richtungen
	 * 
	 * @throws IllegalArgumentException
	 *             falls sich in directions zu wenige oder zu viele Richtungen
	 *             befinden
	 */
	public void move(Direction[] directions) {
		if (directions.length != getTapeCount()) {
			throw new IllegalArgumentException(
					"Es befinden sich in directions zu wenige oder zu viele Richtungen");
		}
		int temp = 0;
		for (ListIterator<Tape<T>> i = tapes.listIterator(0); i.hasNext();) {
			i.next().move(directions[temp]);
			temp++;
		}
	}

	/**
	 * Durch Zeilenumbruch getrennte toString()-Darstellung der einzelnen
	 * Baender.
	 * 
	 * @return String-Darstellung der einzelnen Baender
	 */
	@Override
	public String toString() {
		String s = "";
		for (ListIterator<Tape<T>> i = tapes.listIterator(0); i.hasNext();) {
			s = s + i.next().toString() + "\n";
		}
		return s;
	}

}