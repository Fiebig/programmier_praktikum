package finiteStateMachine.mealy;

import finiteStateMachine.AbstractFiniteStateMachine;
/**
 * Erweitert die Klasse {@code AbstractFiniteStateMachine<I, O>}. Schreibt die
 * Ausgaben ohne Zwischenzeichen wie Leerzeichen oder Zeilenumbruch auf die
 * Konsole.
 * 
 * @author Alexander Fiebig
 * 
 * @param <I>
 *            Eingabealphabet des Automaten
 * @param <O>
 *            Ausgabealphabet des Automaten
 */
public class MealyMachine<I, O> extends AbstractFiniteStateMachine<I, O> {

	/**
	 * Schreibt die Ausgaben ohne Zwischenzeichen wie Leerzeichen oder
	 * Zeilenumbruch auf die Konsole.
	 */
	@Override
	protected void processOutput(O output) {
		System.out.print(output.toString());
	}

	/**
	 * Beispiel:<br>
	 * Simulation eines einfachen Parkautomaten. Es koennen 10-,20-,50-Cent
	 * Stuecke, sowie 1-Euro Muenzen eingeworfen werden. Falls eingeworfener
	 * Betrag ueber Maximalbetrag von 1 Euro liegt wird entsprechende Differenz
	 * zurueckgegeben und automatisch ein Parkschein fuer 1 Euro ausgegeben.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MealyMachine<String, String> ParkAutomat = new MealyMachine<String, String>();
		createStates(ParkAutomat);
		createTransitions(ParkAutomat);
		// Startzustand
		ParkAutomat.setCurrentState("0");
		while (ParkAutomat.isInAcceptedState() == false) {
			// Zufallseingabe fuer Zustandsaenderung
			int eingabe = (int) (Math.random() * 5);
			changeState(ParkAutomat, eingabe);
		}
		if (ParkAutomat.getCurrentState().getName().equals("Wechselgeld")) {
			ParkAutomat.transit("Ausgabe");
		}
		// Endzustand
		ParkAutomat.setCurrentState("0");
	}

	/**
	 * Zustandsaenderung in Abhaengigkeit von eingabe.
	 * 
	 * @param ParkAutomat
	 * @param eingabe
	 */
	private static void changeState(MealyMachine<String, String> ParkAutomat,
			int eingabe) {

		switch (eingabe) {
			case 0 :
				// 10 Cent Muenze einwerfen
				ParkAutomat.transit("10");
				break;
			case 1 :
				// 20 Cent Muenze einwerfen
				ParkAutomat.transit("20");
				break;
			case 2 :
				// 50 Cent Muenze einwerfen
				ParkAutomat.transit("50");
				break;
			case 3 :
				// 1 Euro Muenze einwerfen
				ParkAutomat.transit("100");
				break;
			case 4 :
				// Parkschein anfordern
				ParkAutomat.transit("Ausgabe");
				break;
			default :
				System.out
						.println("eingabe muss eine Zahl zwischen 0 und 4 sein");
				break;

		}
	}

	/**
	 * Erzeugung der Zustaende fuer das Beispiel.
	 * 
	 * @param ParkAutomat
	 */
	private static void createStates(MealyMachine<String, String> ParkAutomat) {
		// Noch kein Geld eingeworfen
		ParkAutomat.addState("0");
		// Insgesamt 10 Cent eingeworfen
		ParkAutomat.addState("10");
		// Insgesamt 20 Cent eingeworfen
		ParkAutomat.addState("20");
		// Insgesamt 30 Cent eingeworfen
		ParkAutomat.addState("30");
		// Insgesamt 40 Cent eingeworfen
		ParkAutomat.addState("40");
		// Insgesamt 50 Cent eingeworfen
		ParkAutomat.addState("50");
		// Insgesamt 60 Cent eingeworfen
		ParkAutomat.addState("60");
		// Insgesamt 70 Cent eingeworfen
		ParkAutomat.addState("70");
		// Insgesamt 80 Cent eingeworfen
		ParkAutomat.addState("80");
		// Insgesamt 90 Cent eingeworfen
		ParkAutomat.addState("90");
		// Insgesamt 1 Euro eingeworfen
		ParkAutomat.addState("100");
		/*
		 * Falls eingeworfener Betrag ueber Maximalbetrag von 1Euro liegt wird
		 * entsprechende Differenz zurueckgegeben
		 */
		ParkAutomat.addState("Wechselgeld");
		ParkAutomat.getState("Wechselgeld").setAccepted(true);
		// Automat gibt Parkschein aus
		ParkAutomat.addState("Ausgabe");
		ParkAutomat.getState("Ausgabe").setAccepted(true);
	}

	/**
	 * Erzeugung der Uebergaenge fuer das Beispiel.
	 * 
	 * @param ParkAutomat
	 */
	private static void createTransitions(
			MealyMachine<String, String> ParkAutomat) {

		// Uebergaenge von Zustand 0 aus
		ParkAutomat.addTransition("0", "10", "10", "10CentEingeworfen;");
		ParkAutomat.addTransition("0", "20", "20", "20CentEingeworfen;");
		ParkAutomat.addTransition("0", "50", "50", "50CentEingeworfen;");
		ParkAutomat.addTransition("0", "100", "100", "1EuroEingeworfen;");
		ParkAutomat.addTransition("0", "Ausgabe", "Ausgabe", "KeinParkschein;");

		// Uebergaenge von Zustand 10 aus
		ParkAutomat.addTransition("10", "10", "20", "20CentEingeworfen;");
		ParkAutomat.addTransition("10", "20", "30", "30CentEingeworfen;");
		ParkAutomat.addTransition("10", "50", "60", "60CentEingeworfen;");
		ParkAutomat.addTransition("10", "100", "Wechselgeld",
				"1Euro10CentEingeworfen;10CentWechselgeld;");
		ParkAutomat.addTransition("10", "Ausgabe", "Ausgabe",
				"ParkscheinFuer10Cent;");

		// Uebergaenge von Zustand 20 aus
		ParkAutomat.addTransition("20", "10", "30", "30CentEingeworfen;");
		ParkAutomat.addTransition("20", "20", "40", "40CentEingeworfen;");
		ParkAutomat.addTransition("20", "50", "70", "70CentEingeworfen;");
		ParkAutomat.addTransition("20", "100", "Wechselgeld",
				"1Euro20CentEingeworfen;20CentWechselgeld;");
		ParkAutomat.addTransition("20", "Ausgabe", "Ausgabe",
				"ParkscheinFuer20Cent;");

		// Uebergaenge von Zustand 30 aus
		ParkAutomat.addTransition("30", "10", "40", "40CentEingeworfen;");
		ParkAutomat.addTransition("30", "20", "50", "50CentEingeworfen;");
		ParkAutomat.addTransition("30", "50", "80", "80CentEingeworfen;");
		ParkAutomat.addTransition("30", "100", "Wechselgeld",
				"1Euro30CentEingeworfen;30CentWechselgeld;");
		ParkAutomat.addTransition("30", "Ausgabe", "Ausgabe",
				"ParkscheinFuer30Cent;");

		// Uebergaenge von Zustand 40 aus
		ParkAutomat.addTransition("40", "10", "50", "50CentEingeworfen;");
		ParkAutomat.addTransition("40", "20", "60", "60CentEingeworfen;");
		ParkAutomat.addTransition("40", "50", "90", "90CentEingeworfen;");
		ParkAutomat.addTransition("40", "100", "Wechselgeld",
				"1Euro40CentEingeworfen;40CentWechselgeld;");
		ParkAutomat.addTransition("40", "Ausgabe", "Ausgabe",
				"ParkscheinFuer40Cent;");

		// Uebergaenge von Zustand 50 aus
		ParkAutomat.addTransition("50", "10", "60", "60CentEingeworfen;");
		ParkAutomat.addTransition("50", "20", "70", "70CentEingeworfen;");
		ParkAutomat.addTransition("50", "50", "100", "1EuroEingeworfen;");
		ParkAutomat.addTransition("50", "100", "Wechselgeld",
				"1Euro50CentEingeworfen;50CentWechselgeld;");
		ParkAutomat.addTransition("50", "Ausgabe", "Ausgabe",
				"ParkscheinFuer50Cent;");

		// Uebergaenge von Zustand 60 aus
		ParkAutomat.addTransition("60", "10", "70", "70CentEingeworfen;");
		ParkAutomat.addTransition("60", "20", "80", "80CentEingeworfen;");
		ParkAutomat.addTransition("60", "50", "Wechselgeld",
				"1Euro10CentEingeworfen;10CentWechselgeld;");
		ParkAutomat.addTransition("60", "100", "Wechselgeld",
				"1Euro60CentEingeworfen;60CentWechselgeld;");
		ParkAutomat.addTransition("60", "Ausgabe", "Ausgabe",
				"ParkscheinFuer60Cent;");

		// Uebergaenge von Zustand 70 aus
		ParkAutomat.addTransition("70", "10", "80", "80CentEingeworfen;");
		ParkAutomat.addTransition("70", "20", "90", "90CentEingeworfen;");
		ParkAutomat.addTransition("70", "50", "Wechselgeld",
				"1Euro20CentEingeworfen;20CentWechselgeld;");
		ParkAutomat.addTransition("70", "100", "Wechselgeld",
				"1Euro70CentEingeworfen;70CentWechselgeld;");
		ParkAutomat.addTransition("70", "Ausgabe", "Ausgabe",
				"ParkscheinFuer70Cent;");

		// Uebergaenge von Zustand 80 aus
		ParkAutomat.addTransition("80", "10", "90", "90CentEingeworfen;");
		ParkAutomat.addTransition("80", "20", "100", "1EuroEingeworfen;");
		ParkAutomat.addTransition("80", "50", "Wechselgeld",
				"1Euro30CentEingeworfen;30CentWechselgeld;");
		ParkAutomat.addTransition("80", "100", "Wechselgeld",
				"1Euro80CentEingeworfen;80CentWechselgeld;");
		ParkAutomat.addTransition("80", "Ausgabe", "Ausgabe",
				"ParkscheinFuer80Cent;");

		// Uebergaenge von Zustand 90 aus
		ParkAutomat.addTransition("90", "10", "100", "1EuroEingeworfen;");
		ParkAutomat.addTransition("90", "20", "Wechselgeld",
				"1Euro10CentEingeworfen;10CentWechselgeld;");
		ParkAutomat.addTransition("90", "50", "Wechselgeld",
				"1Euro40CentEingeworfen;40CentWechselgeld;");
		ParkAutomat.addTransition("90", "100", "Wechselgeld",
				"1Euro90CentEingeworfen;90CentWechselgeld;");
		ParkAutomat.addTransition("90", "Ausgabe", "Ausgabe",
				"ParkscheinFuer90Cent;");

		// Uebergaenge von Zustand 100 aus
		ParkAutomat.addTransition("100", "10", "Wechselgeld",
				"1Euro10CentEingeworfen;10CentWechselgeld;");
		ParkAutomat.addTransition("100", "20", "Wechselgeld",
				"1Euro20CentEingeworfen;20CentWechselgeld;");
		ParkAutomat.addTransition("100", "50", "Wechselgeld",
				"1Euro50CentEingeworfen;50CentWechselgeld;");
		ParkAutomat.addTransition("100", "100", "Wechselgeld",
				"2EuroEingeworfen;1EuroWechselgeld;");
		ParkAutomat.addTransition("100", "Ausgabe", "Ausgabe",
				"ParkscheinFuer1Euro;");

		// Uebergaeng von Zustand Wechselgeld aus
		ParkAutomat.addTransition("Wechselgeld", "Ausgabe", "Ausgabe",
				"ParkscheinFuer1Euro;");

	}

}
