package finiteStateMachine.state;

/**
 * Speichert die Informationen zu einem konkreten Übergang.
 * 
 * @author Alexander Fiebig
 * 
 * @param <O>
 *            Ausgabealphabet des zugehoerigen Automaten
 */
public class Transition<O> {
	/**
	 * Zum Uebergang gehoerige Ausgabe.
	 */
	private final O output;
	/**
	 * Name des Nachfolgezustand.
	 */
	private final String targetState;

	/**
	 * Erstellt einen neuen Uebergang. Setzt den Zielzustand auf den Zustand mit
	 * dem Namen targetState und die zugehoerige Ausgabe auf output.
	 * 
	 * @param targetState
	 *            Name des Zielzustand
	 * @param output
	 *            Zugehoerige Ausgabe
	 */
	public Transition(String targetState, O output) {
		this.targetState = targetState;
		this.output = output;
	}

	/**
	 * Liefert den Namen des Zielzustand.
	 * 
	 * @return Name des Zielzustand
	 */
	public String getNextState() {
		if (targetState == null) {
			return null;
		}
		return targetState;
	}

	/**
	 * Liefert die zugehoerige Ausgabe.
	 * 
	 * @return Zugehoerige Ausgabe
	 */
	public O getOutput() {
		return output;
	}

	/**
	 * Liefert einen String der Form [Name des Nachfolgezustandes],[toString()
	 * der Ausgabe] zurück.
	 * 
	 * @return [Name des Nachfolgezustandes],[toString() der Ausgabe]
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(targetState);
		sb.append(",");
		sb.append(output);
		return sb.toString();
	}

}
