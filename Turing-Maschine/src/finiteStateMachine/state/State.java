package finiteStateMachine.state;

import java.util.HashMap;
import java.util.Map;

import finiteStateMachine.AbstractFiniteStateMachine;

/**
 * Stellt einen konkreten, an einen festen Automaten gebundenen Zustand dar.
 * Verwaltet die moeglichen Zustandsuebergaenge und setzt im Falle eines
 * Uebergangs den aktuellen Zustand im zugehoerigen Automaten neu und liefert
 * die zugehoerige Ausgabe zurueck.
 * 
 * @author Alexander Fiebig
 * 
 * @param <I>
 *            Eingabealphabet des zugehoerigen Automaten
 * @param <O>
 *            Ausgabealphabet des zugehoerigen Automaten
 */
public class State<I, O> {
	/**
	 * Zugehoeriger Automat.
	 */
	private final AbstractFiniteStateMachine<I, O> afsm;
	/**
	 * Name des Zustand.
	 */
	private final String name;
	/**
	 * Gibt an, ob dieser Zustand im zugehoerigen Automaten aktzeptiert ist.
	 */
	private boolean accepted;
	/**
	 * Speichert die moeglichen Zustandsuebergaenge.
	 */
	private final Map<I, Transition<O>> transitions;

	/**
	 * Erstellt einen neuen Zustand mit dem Namen name und dem zugehoerigen
	 * Automaten machine. Setzt accepted auf false.
	 * 
	 * @param machine
	 *            Zugehoeriger Automat
	 * @param name
	 *            Name des Zustand
	 */
	public State(AbstractFiniteStateMachine<I, O> machine, String name) {
		this.accepted = false;
		this.afsm = machine;
		this.name = name;
		this.transitions = new HashMap<I, Transition<O>>();
	}

	/**
	 * Liefert den Namen des Zustand.
	 * 
	 * @return Name des Zustand
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Anzahl der Uebergaenge fuer diesen Zustand.
	 * 
	 * @return Anzahl der Uebergaenge fuer diesen Zustand
	 */
	public int getTransitionCount() {
		return transitions.size();
	}

	/**
	 * Liefert den zugehoerigen Automat.
	 * 
	 * @return Zugehoeriger Automat
	 */
	public AbstractFiniteStateMachine<I, O> getMachine() {
		return afsm;
	}

	/**
	 * Kennzeichnet den Zustand als aktzeptiert.
	 * 
	 * @param accepted
	 */
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	/**
	 * Zeigt an, ob der Zustand aktzeptiert ist.
	 * 
	 * @return
	 */
	public boolean isAccepted() {
		return accepted;
	}

	/**
	 * Fuegt einen Uebergang zum Zustand mit dem Namen targetState hinzu, der
	 * bei der Eingabe input ausgefuehrt werden soll. Ist bereits ein Uebergang
	 * für die Eingabe festgelegt wird dieser ueberschrieben.
	 * 
	 * @param input
	 *            Eingabe in den Automaten
	 * @param targetState
	 *            Name des Zielzustand
	 * @param output
	 *            Ausgabe beim Uebergang
	 */
	public void addTransition(I input, String targetState, O output) {
		Transition<O> t = new Transition<O>(targetState, output);
		transitions.put(input, t);
	}

	/**
	 * Liefert den Uebergang, der bei der Eingabe input ausgefuehrt wird. Ist
	 * kein Uebergang für die Eingabe definiert wird null zurueckgegeben.
	 * 
	 * @param input
	 *            Eingabe in den Automaten
	 * @return Uebergang, der bei der Eingabe input ausgeführt wird
	 */
	public Transition<O> getTransition(I input) {
		return transitions.get(input);
	}

	/**
	 * Fuehrt den Zustandsuebergang bei Eingabe von input aus. Hierzu wird in
	 * der zum Zustand gehoerigen Maschine der aktuelle Zustand auf den
	 * entsprechenden Nachfolgezustand gesetzt und die entsprechende Ausgabe
	 * zuruekgegeben.
	 * 
	 * @param input
	 *            Eingabe in den Automaten
	 * @return Zum Uebergang gehoerige Ausgabe
	 * 
	 * @throws IllegalArgumentException
	 *             falls kein Uebergang für die Eingabe input definiert ist
	 */
	public O transit(I input) {
		Transition<O> t = transitions.get(input);
		if (t == null) {
			throw new IllegalArgumentException(
					"Es ist keine Transition fuer die Eingabe definiert");
		}
		afsm.setCurrentState(t.getNextState());
		return t.getOutput();
	}

	/**
	 * Liefert den Namen des Zustand
	 * 
	 * @return Name des Zustand
	 */
	@Override
	public String toString() {
		return name;
	}
}
