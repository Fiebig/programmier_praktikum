package finiteStateMachine;

import java.util.HashMap;
import java.util.Map;

import finiteStateMachine.state.State;
import finiteStateMachine.state.Transition;

/**
 * Stellt einen abstrakten endlichen Automaten dar, der einen Verweis auf seinen
 * aktuellen Zustand und eine Aufzaehlung aller moeglichen Zustaende enthaelt.
 * 
 * @author Alexander Fiebig
 * 
 * @param <I>
 *            Eingabealphabet des Automaten
 * @param <O>
 *            Ausgabealphabet des Automaten
 */
public abstract class AbstractFiniteStateMachine<I, O> {
	/**
	 * Speichert die moeglichen Zustaende des Automaten.
	 */
	protected Map<String, State<I, O>> states = new HashMap<String, State<I, O>>();
	/**
	 * Aktueller Zustand des Automaten.
	 */
	protected State<I, O> currentState;

	/**
	 * Fuegt dem Automaten einen neuen Zustand mit dem Namen state hinzu.
	 * 
	 * @param state
	 *            Name des neuen Zustand
	 * 
	 * @throws IllegalStateException
	 *             falls bereits ein Zustand mit dem Namen state vorhanden ist
	 */
	public void addState(String state) {
		if (this.states.containsKey(state)) {
			throw new IllegalStateException(
					"Es ist bereits ein Zustand mit dem Namen " + state
							+ " vorhanden");
		}
		State<I, O> s = new State<I, O>(this, state);
		states.put(s.getName(), s);
	}

	/**
	 * Liefert den Zustand mit dem Namen state zurueck. Falls kein Zustand mit
	 * dem Namen state vorhanden ist wird null zuruekgegeben.
	 * 
	 * @param state
	 *            Name des gesuchten Zustand
	 * @return Zustand mit dem Namen state oder null
	 */
	public State<I, O> getState(String state) {
		return this.states.get(state);
	}

	/**
	 * Fuegt einen Uebergang von startState nach targetState bei Eingabe input
	 * mit der Ausgabe output hinzu. Der Uebergang selbst wird im Zustand mit
	 * dem Namen startState gespeichert.
	 * 
	 * @param startState
	 *            Name des Ausgangszustand
	 * @param input
	 *            Eingabe in den Automaten
	 * @param targetState
	 *            Name des Zielzustand
	 * @param output
	 *            Ausgabe beim Uebergang
	 * @throws IllegalStateException
	 *             falls einer der beiden Zustaende startState oder targetState
	 *             nicht vorhanden ist
	 */
	public void addTransition(String startState, I input, String targetState,
			O output) {
		State<I, O> sS = this.states.get(startState);
		if (sS == null) {
			throw new IllegalStateException(
					"Es ist kein Zustand mit dem Namen " + startState
							+ " vorhanden");
		}
		if (!this.states.containsKey(targetState)) {
			throw new IllegalStateException(
					"Es ist kein Zustand mit dem Namen " + targetState
							+ " vorhanden");
		}
		sS.addTransition(input, targetState, output);
	}

	/**
	 * Setzt den aktuellen Zustand des Automaten auf den Zustand mit dem Namen
	 * state.
	 * 
	 * @param state
	 *            Name des gewuenschten aktuellen Zustand
	 * 
	 * @throws IllegalStateException
	 *             falls kein Zustand mit dem Namen state vorhanden ist
	 */
	public void setCurrentState(String state) {
		State<I, O> s = this.states.get(state);
		if (s == null) {
			throw new IllegalStateException(
					"Es ist kein Zustand mit dem Namen " + state + " vorhanden");
		}
		currentState = s;
	}

	/**
	 * Liefert den aktuellen Zustand zurueck.
	 * 
	 * @return Aktueller Zustand
	 */
	public State<I, O> getCurrentState() {
		return currentState;
	}

	/**
	 * Gibt an, ob der aktuelle Zustand des Automaten akzeptiert ist. Ist kein
	 * aktueller Zustand gesetzt, wird false zurueckgegeben werden.
	 * 
	 * @return
	 */
	public boolean isInAcceptedState() {
		if (currentState == null) {
			return false;
		}
		return currentState.isAccepted();
	}

	/**
	 * Fuehrt einen Uebergang vom aktuellen Zustand mit der Eingabe input aus.
	 * Der Zustandswechsel selbst ist in der Zustands-Klasse implementiert. Die
	 * zurückgelieferte Ausgabe dieses Zustandsuebergangs wird dann mit der
	 * Methode {@code processOutput(O output)} verarbeitet. Die Verarbeitung der
	 * Ausgabe haengt von dem konkreten Automatentyp ab. Dieser ist in der
	 * abstrakten Klasse natuerlich nicht bekannt, deswegen wird hier dem
	 * Schablonen-Entwurfsmuster entsprechend lediglich die abstrakte Methode
	 * aufgerufen.
	 * 
	 * @param input
	 *            Eingabe in den Automaten
	 * 
	 * @throws IllegalArgumentException
	 *             falls kein Uebergang für die Eingabe definiert ist
	 * @throws IllegalStateException
	 *             falls kein Zustand als aktuell gekennzeichnet ist
	 */
	public void transit(I input) {
		if (currentState == null) {
			throw new IllegalStateException(
					"Es wurde noch kein aktueller Zustand gesetzt");
		}
		O output = currentState.transit(input);
		processOutput(output);
	}

	/**
	 * Liefert den Uebergang zurueck, der bei aktuellem Zustand mit der Eingabe
	 * input ausgefuehrt werden wuerde. Ist kein Uebergang für die Eingabe
	 * definiert wird null zurueckgegeben.
	 * 
	 * @param input
	 *            Eingabe in den Automaten
	 * @return Uebergang, der bei aktuellem Zustand mit der Eingabe input
	 *         ausgefuehrt werden wuerde
	 * 
	 * @throws IllegalStateException
	 *             falls kein aktueller Zustand gesetzt ist
	 */
	public Transition<O> getTransition(I input) {
		if (currentState == null) {
			throw new IllegalStateException(
					"Es wurde noch kein aktueller Zustand gesetzt");
		}
		return currentState.getTransition(input);
	}

	/**
	 * Dies ist die einzige abstrakte Methode in dieser Klasse. Bei konkreten
	 * Implementierungen eines Automatentyps wird hier festgelegt, wie mit der
	 * Ausgabe bei einem Uebergang verfahren werden soll.
	 * 
	 * @param output
	 *            Ausgabe bei einem Uebergang
	 */
	protected abstract void processOutput(O output);
}
