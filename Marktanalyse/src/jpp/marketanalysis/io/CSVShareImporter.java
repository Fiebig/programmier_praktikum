package jpp.marketanalysis.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;

import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;
import jpp.marketanalysis.operationsContainer.FormatOperations;

/**
 * Liest vom Eingabestrom eine Aktiensammlung im zuvor in den Klassen Dataset,
 * Share und ShareCollection definierten CSV-Format ein. Sollten Fehler
 * auftreten, wird wie im Interface beschrieben eine java.io.IOException
 * geworfen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class CSVShareImporter implements ShareImporter {
	/**
	 * Liest vom Eingabestrom eine Aktiensammlung im zuvor in den Klassen
	 * Dataset, Share und ShareCollection definierten CSV-Format ein.
	 * 
	 * @throws IOException
	 *             falls Fehler auftreten
	 * @return Collection mit den eingelesenen Aktien
	 */
	@Override
	public Collection<Share> load(InputStream in) throws IOException {

		try (BufferedReader data = new BufferedReader(new InputStreamReader(in))) {

			String aktuelleZeile = data.readLine();
			if (!aktuelleZeile.trim().equals("date;open;high;low;close")) {
				throw new IOException("Ung�ltige Kopfzeile");
			}

			Collection<Share> shares = new ArrayList<Share>();
			Collection<Dataset> dataSets = new ArrayList<Dataset>();
			String name = null;

			while ((aktuelleZeile = data.readLine()) != null) {
				String[] split = aktuelleZeile.split(";");

				if (split.length == 2
						&& aktuelleZeile.trim().equals(
								"SHARE;" + split[1].trim() + ";;;")) {

					if (name != null) {
						Share share = new Share(name, dataSets);
						shares.add(share);
						dataSets.clear();
					}
					name = split[1].trim();
				}
				else {
					dataSets.add(createDataset(split));
				}
			}

			Share share = new Share(name.trim(), dataSets);
			shares.add(share);
			return shares;

		}
		catch (NullPointerException ex) {
			throw new IOException("Unvollstaendige Daten");
		}
		catch (IllegalArgumentException ex) {
			throw new IOException(ex.getMessage());
		}
	}

	private Dataset createDataset(String[] dataSplit) throws IOException {
		try {
			if (dataSplit.length != 5) {
				throw new IOException("Ung�ltige Data- bzw. Sharezeile");
			}

			LocalDate date = FormatOperations.parseDate(dataSplit[0].trim());
			double open = Double.valueOf(dataSplit[1].trim());
			double high = Double.valueOf(dataSplit[2].trim());
			double low = Double.valueOf(dataSplit[3].trim());
			double close = Double.valueOf(dataSplit[4].trim());

			return new Dataset(date, open, high, low, close);

		}
		catch (DateTimeParseException | NumberFormatException ex) {
			throw new IOException("Ung�ltige Datazeile");
		}
	}

	/**
	 * Schreibt sc auf out im CSV-Format.
	 * 
	 * @param sc
	 *            Gegebene ShareCollection
	 * @param out
	 *            Outputstream auf den geschrieben werden soll
	 */
	@Override
	public void save(ShareCollection sc, OutputStream out) throws IOException {
		String data = sc.toString();
		out.write(data.getBytes());
		out.flush();
	}

}
