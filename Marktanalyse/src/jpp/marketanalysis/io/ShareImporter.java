package jpp.marketanalysis.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;

/**
 * Repräsentiert einen Typ, der Aktiendaten von einer Quelle einlesen kann.
 * Aktiendaten können in den unterschiedlichsten Formaten vorliegen, daher ist
 * es nicht sinnvoll sich auf ein einziges Format, bzw. eine einzige Quelle
 * festzulegen.
 * 
 * @author Alexander Fiebig
 * 
 */
public interface ShareImporter {
	/**
	 * Liest eine Sammlung von Aktiendaten vom java.io.InputStream ein.
	 * 
	 * @param in
	 *            Inputstream der gelesen wird
	 * @return Entsprechende Aktiensammlung
	 * @throws IOException
	 *             falls ein Fehler auftritt
	 */
	public Collection<Share> load(InputStream in) throws IOException;

	/**
	 * 
	 * @param sc
	 * @param out
	 * @throws IOException
	 */
	public void save(ShareCollection sc, OutputStream out) throws IOException;
}
