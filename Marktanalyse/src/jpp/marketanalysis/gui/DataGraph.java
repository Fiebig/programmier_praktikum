package jpp.marketanalysis.gui;

import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.operationsContainer.FormatOperations;
import jpp.marketanalysis.operationsContainer.SortOperations;

/**
 * Erstellt einen Graphen der die Tagesendwerte der ausgewaehlten Aktie
 * darstellt.
 * 
 * @author Alexander Fiebig
 * 
 */
public class DataGraph extends Stage {
	/**
	 * Linie des Graphen.
	 */
	private final Series<String, Number> series;
	/**
	 * Titel des Graphen.
	 */
	private final String title;

	/**
	 * Erstellt einen Graphen fuer die uebergebene Aktie.
	 * 
	 * @param aktie
	 *            Uebergebene Aktie
	 */

	public DataGraph(Share aktie) {
		series = new XYChart.Series<>();
		String title = "Tagesendwerte von " + aktie.getName();
		this.title = title;
		series.setName(aktie.getName());
		SortOperations.sortDataByDate(true, aktie.getData());
		for (Dataset dataTemp : aktie.getData()) {
			String date = FormatOperations.formatDate(dataTemp.getDate());
			series.getData().add(new XYChart.Data<>(date, dataTemp.getClose()));
		}
		createStage();
	}

	private void createStage() {
		this.setTitle("Data Graph");
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setAutoRanging(true);
		xAxis.setLabel("Datum");
		final NumberAxis yAxis = new NumberAxis();
		yAxis.setAutoRanging(true);
		yAxis.setForceZeroInRange(false);
		yAxis.setLabel("Tagesendwert");

		final LineChart<String, Number> lineChart = new LineChart<String, Number>(
				xAxis, yAxis);

		lineChart.setTitle(title);

		Scene scene = new Scene(lineChart, 800, 600);
		scene.getStylesheets()
				.add("file:///C:/Users/Alex/workspace/Marktanalyse/marketanalyser_Graph.css");
		lineChart.getData().add(series);

		this.setScene(scene);
	}
}
