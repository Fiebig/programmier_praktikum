package jpp.marketanalysis.gui;

import javafx.application.Application;

public class Main {

	public static void main(String[] args) {
		Application.launch(MarketAnalyserGui.class, args);
	}
}
