package jpp.marketanalysis.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.operationsContainer.SortOperations;

/**
 * Erstellt ein Fenster mit einer Tabelle zur Anzeige von Datensaetzen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class DataTable extends Stage {
	/**
	 * Titel der Tabelle.
	 */
	private final String title;
	/**
	 * Daten fuer die Tabelle.
	 */
	private final ObservableList<TableData> tableData;

	/**
	 * Erstellt eine Tabelle mit den Datensaetzen der uebergebenen Aktie.
	 * 
	 * @param aktie
	 *            Uebergebene Aktie
	 */
	public DataTable(Share aktie) {
		title = "Daten von " + aktie.getName();
		SortOperations.sortDataByDate(false, aktie.getData());
		tableData = FXCollections.observableArrayList();
		for (Dataset dataTemp : aktie.getData()) {
			String[] split = dataTemp.toString().split(";");
			TableData td = new TableData(split[0], split[1], split[2],
					split[3], split[4]);
			tableData.add(td);
		}
		createStage();
	}

	/**
	 * Erstellt eine Tabelle mit dem uebergebenen Datensatz.
	 * 
	 * @param aktie
	 *            Uebergebener Datensatz
	 */
	public DataTable(Dataset ds, String title) {
		this.title = title;
		tableData = FXCollections.observableArrayList();
		String[] split = ds.toString().split(";");
		TableData td = new TableData(split[0], split[1], split[2], split[3],
				split[4]);
		tableData.add(td);
		createStage();
	}

	private void createStage() {
		this.setTitle("DataTable");
		this.setWidth(700);
		this.setHeight(800);

		final Label label = new Label(title);
		label.setFont(new Font("Arial", 20));

		final TableView<TableData> table = new TableView<TableData>();
		table.setEditable(false);
		table.setMinSize(550, 650);
		table.setMaxSize(550, 650);

		TableColumn<TableData, String> date = new TableColumn<>("Tag");
		date.setMinWidth(50);
		date.setCellValueFactory(new PropertyValueFactory<>("date"));

		TableColumn<TableData, String> open = new TableColumn<>(
				"Eröffnungswert");
		open.setMinWidth(150);
		open.setCellValueFactory(new PropertyValueFactory<>("open"));

		TableColumn<TableData, String> high = new TableColumn<>("Höchstwert");
		high.setMinWidth(100);
		high.setCellValueFactory(new PropertyValueFactory<>("high"));

		TableColumn<TableData, String> low = new TableColumn<>("Tiefstwert");
		low.setMinWidth(100);
		low.setCellValueFactory(new PropertyValueFactory<>("low"));

		TableColumn<TableData, String> close = new TableColumn<>("Endwert");
		close.setMinWidth(100);
		close.setCellValueFactory(new PropertyValueFactory<>("close"));

		table.setItems(tableData);
		table.getColumns().add(date);
		table.getColumns().add(open);
		table.getColumns().add(high);
		table.getColumns().add(low);
		table.getColumns().add(close);

		final VBox vbox = new VBox(5);
		vbox.setAlignment(Pos.CENTER);
		vbox.setPadding(new Insets(20, 0, 0, 70));
		vbox.getChildren().addAll(label, table);

		Group root = new Group();
		root.getChildren().add(vbox);

		Scene scene = new Scene(root);
		this.setScene(scene);
		this.setResizable(false);

	}

	public static class TableData {

		private final SimpleStringProperty date;
		private final SimpleStringProperty open;
		private final SimpleStringProperty high;
		private final SimpleStringProperty low;
		private final SimpleStringProperty close;

		private TableData(String date, String open, String high, String low,
				String close) {
			this.date = new SimpleStringProperty(date);
			this.open = new SimpleStringProperty(open);
			this.high = new SimpleStringProperty(high);
			this.low = new SimpleStringProperty(low);
			this.close = new SimpleStringProperty(close);
		}

		public String getDate() {
			return date.get();
		}

		public void setDate(String date) {
			this.date.set(date);
		}

		public String getOpen() {
			return open.get();
		}

		public void setOpen(String open) {
			this.open.set(open);
		}

		public String getHigh() {
			return high.get();
		}

		public void setHigh(String high) {
			this.high.set(high);
		}

		public String getLow() {
			return low.get();
		}

		public void setLow(String low) {
			this.low.set(low);
		}

		public String getClose() {
			return close.get();
		}

		public void setClose(String close) {
			this.close.set(close);
		}

	}

}
