package jpp.marketanalysis.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.format.DateTimeParseException;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jpp.marketanalysis.analysis.ShareCollectionAnalyser;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;
import jpp.marketanalysis.io.CSVShareImporter;
import jpp.marketanalysis.io.ShareImporter;
import jpp.marketanalysis.operationsContainer.FormatOperations;

/**
 * Erstellt eine GUI zum Analysieren von Aktiendaten.
 * 
 * @author Alexander Fiebig
 * 
 */
public class MarketAnalyserGui extends Application {
	/**
	 * Die momentan eingelesene Aktiensammlung.
	 */
	private ShareCollectionAnalyser sca;
	/**
	 * Die momentan in der Combobox ausgewaehlte Aktie.
	 */
	private String ausgewählteAktie;

	private final ShareImporter si = new CSVShareImporter();

	@Override
	public void start(final Stage primaryStage) {
		primaryStage.setTitle("Marketanalyser");

		final Text tf2 = new Text("Local Average:");
		tf2.setFont(Font.font("Arial", FontWeight.BOLD, 16));

		final TextField localAvgTextField = new TextField();
		localAvgTextField.setText("Noch keine Aktie ausgewählt");
		localAvgTextField.setMinSize(220, 20);
		localAvgTextField.setEditable(false);

		final ComboBox<String> aktienComboBox = new ComboBox<String>();
		aktienComboBox.setMinSize(200, 20);
		aktienComboBox.setVisibleRowCount(3);
		aktienComboBox.setPromptText("Aktie auswählen");
		aktienComboBox.valueProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> ov,
							String oldShare, String newShare) {
						ausgewählteAktie = newShare;
						Double avg = sca.getLocalAverage(ausgewählteAktie);
						localAvgTextField.setText(FormatOperations
								.formatValue(avg));
					}
				});

		final Button loadButton = new Button("Load");
		loadButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				try {
					Stage stage = new Stage();
					FileChooser fileChooser = new FileChooser();
					File file = fileChooser.showOpenDialog(stage);
					if (file != null) {
						InputStream shareData = new FileInputStream(file);
						sca = new ShareCollectionAnalyser(new ShareCollection(
								si.load(shareData)));
						for (Share share : sca.getShareCollection()) {
							if (aktienComboBox.getItems().contains(
									share.getName()) == false) {
								aktienComboBox.getItems().add(share.getName());
							}
						}
					}
				}
				catch (IOException ex) {
					System.err.println("Ungültige Datei: " + ex.getMessage());
				}
				catch (IllegalArgumentException ex) {
					System.err.println("Ungültige Datei: " + ex.getMessage());
				}
			}
		});

		final Button saveButton = new Button("Save");

		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				try {
					if (sca != null) {
						Stage stage = new Stage();
						FileChooser fileChooser = new FileChooser();
						// Set extension filter
						FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
								"TXT files (*.txt)", "*.txt");
						fileChooser.getExtensionFilters().add(extFilter);
						// Create or select file
						File selectedFile = fileChooser.showSaveDialog(stage);
						if (selectedFile != null) {
							OutputStream out = new FileOutputStream(
									selectedFile);
							si.save(sca.getShareCollection(), out);
						}
					}
				}
				catch (IOException ex) {
					System.err.println(ex.getMessage());
				}
			}
		});

		final Button btn = new Button();
		btn.setText("Show Sharedata");
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (sca != null) {
					try {
						Share gesucht = sca.getShareCollection().getShare(
								ausgewählteAktie);
						DataTable table = new DataTable(gesucht);
						table.show();
					}
					catch (IllegalArgumentException ex) {
						System.err.println("Noch keine Aktie ausgewählt");
					}
				}
				else {
					System.err.println("Noch keine Datei eingelesen");
				}
			}
		});

		final Button btn2 = new Button();
		btn2.setText("Show Minimum");
		btn2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (sca != null) {
					try {
						Dataset min = sca.getLocalMinimum(ausgewählteAktie);
						String title = "Minimum von " + ausgewählteAktie;
						DataTable table = new DataTable(min, title);
						table.show();
					}
					catch (IllegalArgumentException ex) {
						System.err.println("Noch keine Aktie ausgewählt");
					}
				}
				else {
					System.err.println("Noch keine Datei eingelesen");
				}
			}
		});

		final Button btn3 = new Button();
		btn3.setText("Show Maximum");
		btn3.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (sca != null) {
					try {
						Dataset max = sca.getLocalMaximum(ausgewählteAktie);
						String title = "Maximum von " + ausgewählteAktie;
						DataTable table = new DataTable(max, title);
						table.show();
					}
					catch (IllegalArgumentException ex) {
						System.err.println("Noch keine Aktie ausgewählt");
					}
				}
				else {
					System.err.println("Noch keine Datei eingelesen");
				}
			}
		});

		final Button btn4 = new Button();
		btn4.setText("Draw Sharegraph");
		btn4.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (sca != null) {
					try {
						Share gesucht = sca.getShareCollection().getShare(
								ausgewählteAktie);
						DataGraph graph = new DataGraph(gesucht);
						graph.show();
					}
					catch (IllegalArgumentException ex) {
						System.err.println("Noch keine Aktie ausgewählt");
					}
				}
				else {
					System.err.println("Noch keine Datei eingelesen");
				}
			}
		});

		final Text tf3 = new Text("Global Average:");
		tf3.setFont(Font.font("Arial", FontWeight.BOLD, 16));

		final TextField globalAvgTextField = new TextField();
		globalAvgTextField.setText("Enter Date in Format dd.MM.yyyy");
		globalAvgTextField.setMinSize(250, 20);
		globalAvgTextField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (sca != null) {
					try {
						String date = globalAvgTextField.getText();
						double av = sca.getGlobalAverage(FormatOperations
								.parseDate(date));
						globalAvgTextField.setText(FormatOperations
								.formatValue(av));

					}
					catch (DateTimeParseException ex) {
						globalAvgTextField.setText("Falsches Format");
					}
				}
				else {
					globalAvgTextField.setText("Noch keine Datei eingelesen");
				}
			}
		});

		final AnchorPane main = new AnchorPane();

		HBox hbButtons = new HBox();
		hbButtons.setSpacing(30);
		hbButtons.setPadding(new Insets(10, 20, 10, 20));
		hbButtons.getChildren().addAll(loadButton, saveButton);

		HBox hbButtons2 = new HBox();
		hbButtons2.setSpacing(20);
		hbButtons2.setPadding(new Insets(10, 20, 10, 20));
		hbButtons2.getChildren().addAll(aktienComboBox, btn, btn2, btn3, btn4);

		HBox hbButtons3 = new HBox();
		hbButtons3.setSpacing(20);
		hbButtons3.setPadding(new Insets(10, 20, 10, 20));
		hbButtons3.getChildren().addAll(tf2, localAvgTextField, tf3,
				globalAvgTextField);

		main.getChildren().addAll(hbButtons, hbButtons2, hbButtons3);
		AnchorPane.setTopAnchor(hbButtons, 5.0);
		AnchorPane.setTopAnchor(hbButtons2, 75.0);
		AnchorPane.setTopAnchor(hbButtons3, 145.0);

		primaryStage.setScene(new Scene(main, 900, 250, Color.DEEPSKYBLUE));
		primaryStage.show();
	}

}
