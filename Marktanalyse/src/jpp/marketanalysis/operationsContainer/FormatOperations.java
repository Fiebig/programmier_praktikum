package jpp.marketanalysis.operationsContainer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FormatOperations {

	private static DateTimeFormatter dateFormat;
	private static DecimalFormat valueFormat;

	static {
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
		dfs.setDecimalSeparator('.');
		valueFormat = new DecimalFormat("0.00", dfs);
		dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	}

	/**
	 * Formatiert value auf 2 Nachkommastellen genau.
	 * 
	 * @param value
	 * @return
	 */
	public static String formatValue(double value) {
		return valueFormat.format(value);
	}

	/**
	 * Formatiert date auf dd.MM.yyyy
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDate(LocalDate date) {
		return dateFormat.format(date);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static LocalDate parseDate(String date)
			throws DateTimeParseException {
		return LocalDate.parse(date, dateFormat);
	}

}
