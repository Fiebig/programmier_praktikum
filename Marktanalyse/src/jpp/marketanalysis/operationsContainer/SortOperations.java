package jpp.marketanalysis.operationsContainer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jpp.marketanalysis.analysis.DatasetHighComparator;
import jpp.marketanalysis.analysis.DatasetLowComparator;
import jpp.marketanalysis.analysis.DayComparator.DatasetDayComparator;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;

public class SortOperations {

	/**
	 * Sortiert die Daten nach dem Datum auf- bzw. absteigend.
	 * 
	 * @param aufsteigend
	 *            true, wenn aufsteigend sortiert werden soll
	 */
	public static void sortDataByDate(boolean aufsteigend, List<Dataset> data) {
		if (aufsteigend == false) {
			Collections.sort(data,
					Collections.reverseOrder(new DatasetDayComparator()));
		}
		else {
			Collections.sort(data, new DatasetDayComparator());
		}
	}

	/**
	 * Sortiert die Daten nach dem Tiefstwert auf- bzw. absteigend.
	 * 
	 * @param aufsteigend
	 *            true, wenn aufsteigend sortiert werden soll
	 */
	public static void sortDataByLow(boolean aufsteigend, List<Dataset> data) {
		if (aufsteigend == false) {
			Collections.sort(data,
					Collections.reverseOrder(new DatasetLowComparator()));
		}
		else {
			Collections.sort(data, new DatasetLowComparator());
		}
	}

	/**
	 * Sortiert die Daten nach dem Höchstwert auf- bzw. absteigend.
	 * 
	 * @param aufsteigend
	 *            true, wenn aufsteigend sortiert werden soll
	 */
	public static void sortDataByHigh(boolean aufsteigend, List<Dataset> data) {

		if (aufsteigend == false) {
			Collections.sort(data,
					Collections.reverseOrder(new DatasetHighComparator()));
		}
		else {
			Collections.sort(data, new DatasetHighComparator());
		}

	}

	/**
	 * Sortiert die Aktien alphabetisch nach dem Namen.
	 * 
	 */
	public static void sortSharesByName(List<Share> shares) {

		Collections.sort(shares, new Comparator<Share>() {
			@Override
			public int compare(Share a, Share b) {
				return a.getName().compareTo(b.getName());

			}
		});
	}
}
