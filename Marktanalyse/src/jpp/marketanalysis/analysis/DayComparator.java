package jpp.marketanalysis.analysis;

import java.time.LocalDate;
import java.util.Comparator;

import jpp.marketanalysis.data.Dataset;

/**
 * Vergleicht zwei LocalDate-Objekte ausschlie�lich anhand des Jahres, Monats
 * und Tages.
 * 
 * @author Alexander Fiebig
 * 
 */
public class DayComparator implements Comparator<LocalDate> {

	@Override
	public int compare(LocalDate a, LocalDate b) {

		if (a.getYear() > b.getYear()) {
			return 1;
		}
		if (a.getYear() == b.getYear()) {
			if (a.getMonthValue() > b.getMonthValue()) {
				return 1;
			}
			if (a.getMonthValue() == b.getMonthValue()) {
				if (a.getDayOfMonth() > b.getDayOfMonth()) {
					return 1;
				}
				if (a.getDayOfMonth() == b.getDayOfMonth()) {
					return 0;
				}
			}
		}
		return -1;
	}

	/**
	 * Vergleicht zwei Dataset-Objekte ausschlie�lich anhand ihres Datums.
	 * 
	 * @author Alexander Fiebig
	 * 
	 */
	public static class DatasetDayComparator implements Comparator<Dataset> {

		private static DayComparator dc = new DayComparator();

		@Override
		public int compare(Dataset a, Dataset b) {
			return dc.compare(a.getDate(), b.getDate());
		}
	}
}