package jpp.marketanalysis.analysis;

import java.util.Comparator;

import jpp.marketanalysis.analysis.DayComparator.DatasetDayComparator;
import jpp.marketanalysis.data.Dataset;

/**
 * Vergleicht zwei Dataset-Objekte anhand der Höchstwerte (high). Sollten die
 * Höchstwerte übereinstimmen, so werden die beiden Datasets anhand der Tage
 * verglichen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class DatasetHighComparator implements Comparator<Dataset> {

	@Override
	public int compare(Dataset a, Dataset b) {
		if (a.getHigh() < b.getHigh()) {
			return -1;
		}
		if (a.getHigh() > b.getHigh()) {
			return 1;
		}

		DatasetDayComparator dc = new DatasetDayComparator();
		return dc.compare(a, b);
	}
}
