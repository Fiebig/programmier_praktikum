package jpp.marketanalysis.analysis;

import java.util.Comparator;

import jpp.marketanalysis.analysis.DayComparator.DatasetDayComparator;
import jpp.marketanalysis.data.Dataset;

/**
 * Vergleicht zwei Dataset-Objekte anhand der Tiefstwerte (low). Sollten die
 * Tiefstwerte übereinstimmen, so werden die beiden Datasets anhand der Tage
 * verglichen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class DatasetLowComparator implements Comparator<Dataset> {

	@Override
	public int compare(Dataset a, Dataset b) {
		if (a.getLow() < b.getLow()) {
			return -1;
		}
		if (a.getLow() > b.getLow()) {
			return 1;
		}

		DatasetDayComparator dc = new DatasetDayComparator();
		return dc.compare(a, b);
	}

}
