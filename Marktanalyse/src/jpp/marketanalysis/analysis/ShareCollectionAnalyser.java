package jpp.marketanalysis.analysis;

import java.time.LocalDate;

import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;

/**
 * Kann Berechnungen auf einer ShareCollection durchf�hren.
 * 
 * @author Alexander Fiebig
 * 
 */
public class ShareCollectionAnalyser {
	/**
	 * Aktien, die analysiert werden sollen
	 */
	private final ShareCollection sc;

	/**
	 * Initialisiert einen ShareCollectionAnalyser mit einer ShareCollection,
	 * auf der die Analysen durchgef�hrt werden sollen
	 * 
	 * @param shares
	 *            ShareCollection mit den zu analysierenden Aktien
	 */
	public ShareCollectionAnalyser(ShareCollection shares) {
		this.sc = shares;

	}

	/**
	 * Liefert die ShareCollection, die analysiert wird.
	 * 
	 * @return
	 */
	public ShareCollection getShareCollection() {
		return sc;
	}

	/**
	 * Liefert das Dataset-Objekt der Aktie mit dem Namen name, dass den
	 * h�chsten Wert (high) der Aktie darstellt.
	 * 
	 * @param name
	 *            Name der Aktie
	 * @return Dataset-Objekt, dass den h�chsten Wert (high) der Aktie
	 *         darstellt.
	 * 
	 * @throws IllegalArgumentException
	 *             falls keine Aktie mit dem Namen name vorhanden ist
	 */
	public Dataset getLocalMaximum(String name) throws IllegalArgumentException {
		Share share = sc.getShare(name);
		double maxHigh = Double.NEGATIVE_INFINITY;
		Dataset result = null;

		for (Dataset temp : share.getData()) {
			if (temp.getHigh() > maxHigh) {
				maxHigh = temp.getHigh();
				result = temp;
			}
		}
		return result;
	}

	/**
	 * Liefert das Dataset-Objekt der Aktie mit dem Namen name, dass den
	 * niedrigsten Wert (low) der Aktie darstellt.
	 * 
	 * @param name
	 *            Name der Aktie
	 * @return Dataset-Objekt, dass den niedrigsten Wert (low) der Aktie
	 *         darstellt.
	 * 
	 * @throws IllegalArgumentException
	 *             falls keine Aktie mit dem Namen name vorhanden ist
	 */
	public Dataset getLocalMinimum(String name) throws IllegalArgumentException {
		Share share = sc.getShare(name);
		double minLow = Double.POSITIVE_INFINITY;
		Dataset result = null;

		for (Dataset temp : share.getData()) {
			if (temp.getLow() < minLow) {
				minLow = temp.getLow();
				result = temp;
			}
		}
		return result;
	}

	/**
	 * Liefert den Mittelwert der Tagesendwerte (close) der Aktie mit dem Namen
	 * name.
	 * 
	 * @param name
	 *            Name der Aktie
	 * @return Mittelwert der Tagesendwerte (close)
	 * 
	 * @throws IllegalArgumentException
	 *             falls keine Aktie mit dem Namen name vorhanden ist
	 */
	public double getLocalAverage(String name) throws IllegalArgumentException {
		Share share = sc.getShare(name);
		double anzDatasets = share.getData().size();
		double sumClose = 0;

		for (Dataset temp : share.getData()) {
			sumClose += temp.getClose();
		}
		if (anzDatasets == 0) {
			return 0;
		}
		return sumClose / anzDatasets;

	}

	/**
	 * Liefert den Durchschnitt der Tagesendwerte (close) aller Aktien am Tag
	 * day. Hat eine Aktie an diesem Tag keinen Datensatz, wird sie ingoriert.
	 * K�nnen keine Datens�tze f�r diesen Tag gefunden werden, so wird 0
	 * zur�ckgegeben.
	 * 
	 * @param day
	 *            Tag f�r den der Durchschnitt berechnet werden soll
	 * @return Durchschnitt der Tagesendwerte (close) aller Aktien am Tag day
	 */
	public double getGlobalAverage(LocalDate day) {
		DayComparator dc = new DayComparator();
		double anzDatasets = 0;
		double sumClose = 0;
		for (Share share : sc) {
			for (Dataset temp : share.getData()) {
				if (dc.compare(temp.getDate(), day) == 0) {
					anzDatasets++;
					sumClose += temp.getClose();
				}
			}
		}
		if (anzDatasets == 0) {
			return 0;
		}
		return sumClose / anzDatasets;
	}

}
