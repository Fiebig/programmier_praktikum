package jpp.marketanalysis.console;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.time.format.DateTimeParseException;
import java.util.Collection;

import jpp.marketanalysis.analysis.ShareCollectionAnalyser;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;
import jpp.marketanalysis.io.CSVShareImporter;
import jpp.marketanalysis.operationsContainer.FormatOperations;

/**
 * Erlaubt Auswertung von Aktiendaten auf der Konsole
 * 
 * @author Alexander Fiebig
 */
public class MarketAnalyserConsole {

	/**
	 * Startet das Programm. Ben�tgt Pfad zu einer Datei in der die Aktiendaten
	 * vorliegen.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try (InputStream shareData = new FileInputStream(
				"C:/Users/Alex/workspace/Marktanalyse/testShares.txt")) {
			MarketAnalyserConsole.run(System.in, System.out, System.err,
					shareData);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Startet den Dialog mit dem Nutzer.
	 * 
	 * @param in
	 *            Benutzereingabestrom
	 * @param out
	 *            Standardausgabestrom
	 * @param err
	 *            Fehlerausgabestrom
	 * @param shareData
	 *            Eingabestrom der die Aktiendaten bereitstellt
	 * @throws IOException
	 *             falls beim lesen oder schreiben auf den Streams Fehler
	 *             auftreten
	 */
	public static void run(InputStream in, OutputStream out, OutputStream err,
			InputStream shareData) throws IOException {

		out.write("Welcome to marketanalyser!\n".getBytes());
		out.flush();
		ShareCollection sc = null;
		try {
			CSVShareImporter csv = new CSVShareImporter();
			Collection<Share> shares = csv.load(shareData);
			sc = new ShareCollection(shares);
		}
		catch (IOException | IllegalArgumentException e) {
			err.write(e.getMessage().getBytes());
			err.flush();
		}
		if (sc != null) {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			try {
				mainstep(out, err, new ShareCollectionAnalyser(sc), reader);
			}
			finally {
				reader.close();
			}
		}
	}

	private static void mainstep(OutputStream out, OutputStream err,
			ShareCollectionAnalyser sca, BufferedReader reader)
			throws IOException {

		String head = "\n1. list shares\n2. local max\n3. local min\n4. local average\n5. global average\n6. quit\n\nPlease choose:\n";
		out.write(head.getBytes());
		out.flush();

		switch (reader.readLine()) {
		case "1":
			sidestep1(out, sca);
			mainstep(out, err, sca, reader);
			break;
		case "2":
			sidestep2_3_4(out, err, reader, sca, 2);
			mainstep(out, err, sca, reader);
			break;
		case "3":
			sidestep2_3_4(out, err, reader, sca, 3);
			mainstep(out, err, sca, reader);
			break;
		case "4":
			sidestep2_3_4(out, err, reader, sca, 4);
			mainstep(out, err, sca, reader);
			break;
		case "5":
			sidestep5(out, err, reader, sca);
			mainstep(out, err, sca, reader);
			break;
		case "6":
			out.write("Programm beendet.\n".getBytes());
			out.flush();
			break;
		default:
			err.write("Falsche Eingabe! Eingabe muss eine Zahl zwischen 1 und 6 sein.\n"
					.getBytes());
			err.flush();
			mainstep(out, err, sca, reader);
			break;
		}
	}

	private static void sidestep1(OutputStream out, ShareCollectionAnalyser sca)
			throws IOException {
		StringBuilder sb = new StringBuilder("Known shares:\n");
		int index = 0;
		for (Share share : sca.getShareCollection()) {
			sb.append(++index + ". " + share.getName() + "\n");
		}
		out.write(sb.toString().getBytes());
		out.flush();
	}

	private static void sidestep2_3_4(OutputStream out, OutputStream err,
			BufferedReader reader, ShareCollectionAnalyser sca, int selection)
			throws IOException {
		try {
			out.write("Share number:\n".getBytes());
			out.flush();
			int eingabe = Integer.parseInt(reader.readLine());
			if (eingabe > sca.getShareCollection().size() || eingabe < 1) {
				err.write(("Falsche Eingabe! Eingabe muss eine Zahl zwischen 1 und "
						+ sca.getShareCollection().size() + " sein.\n")
						.getBytes());
				err.flush();
			}
			else {
				Share selectedShare = sca.getShareCollection().get(eingabe - 1);
				if (selection == 2 || selection == 3) {
					Dataset ds = (selection == 2) ? sca
							.getLocalMaximum(selectedShare.getName()) : sca
							.getLocalMinimum(selectedShare.getName());
					out.write((selectedShare.getName() + ": " + ds.toString() + "\n")
							.getBytes());
					out.flush();
				}
				else {// localAv
					double av = sca.getLocalAverage(selectedShare.getName());
					String fin = FormatOperations.formatValue(av) + "\n";
					out.write(fin.getBytes());
					out.flush();
				}
			}
		}
		catch (NumberFormatException e) {
			err.write("Falsche Eingabe! Eingabe muss eine Zahl sein.\n"
					.getBytes());
			err.flush();
		}
	}

	private static void sidestep5(OutputStream out, OutputStream err,
			BufferedReader reader, ShareCollectionAnalyser sca)
			throws IOException {
		try {
			out.write("Day:\n".getBytes());
			out.flush();
			double av = sca.getGlobalAverage(FormatOperations.parseDate(reader
					.readLine()));
			out.write((FormatOperations.formatValue(av) + "\n").getBytes());
			out.flush();
		}
		catch (DateTimeParseException ex) {
			// Eingabe entspricht keinem Datum
			err.write("Falsche Eingabe! Eingabe muss ein Datum im Format dd.MM.yyyy sein.\n"
					.getBytes());
			err.flush();
		}
	}

}
