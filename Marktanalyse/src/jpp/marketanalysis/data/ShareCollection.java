package jpp.marketanalysis.data;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * Verwaltet die Aktien.
 * 
 * @author Alexander Fiebig
 * 
 */
public class ShareCollection implements Iterable<Share> {
	/**
	 * Aktien, die zur Sammlung geh�ren
	 */
	private final TreeMap<String, Share> shares;

	/**
	 * Initialisiert eine leere Aktiensammlung
	 */
	public ShareCollection() {
		this.shares = new TreeMap<>();
	}

	/**
	 * Initialisiert eine Aktiensammlung und f�gt die in shares �bergebenen
	 * Aktien ein.
	 * 
	 * @param shares
	 *            Aktien der Aktiensammlung
	 * 
	 * @throws IllegalArgumentException
	 *             falls in shares Aktien doppelt vorkommen
	 */
	public ShareCollection(Collection<Share> shares) {
		this.shares = new TreeMap<>();
		for (Share share : shares) {
			if (add(share) == false) {
				throw new IllegalArgumentException("Aktien kommen doppelt vor!");
			}
		}
	}

	/**
	 * Liefert einen Iterator der �ber die Aktien in alphabetischer Reihenfolge
	 * der Namen iteriert.
	 * 
	 * @return Iterator der �ber die Aktien der Sammlung iteriert
	 */
	@Override
	public Iterator<Share> iterator() {
		return shares.values().iterator();
	}

	/**
	 * Liefert die Aktiensammlung als Array nach Aktiennamen sortiert.
	 * 
	 * @return Aktiensammlung als Array
	 */
	public Share[] toArray() {
		return shares.values().toArray(new Share[0]);
	}

	/**
	 * Liefert die Aktie mit dem Namen name.
	 * 
	 * @param name
	 *            Name der Aktie
	 * @return Aktie mit dem Namen name
	 * 
	 * @throws IllegalArgumentException
	 *             falls keine Aktie mit dem Namen name vorhanden ist
	 */
	public Share getShare(String name) throws IllegalArgumentException {
		Share share = shares.get(name);
		if (share == null) {
			throw new IllegalArgumentException(
					"Es ist keine Aktie mit dem Namen " + name
							+ " in der Sammlung vorhanden");
		}
		return share;
	}

	/**
	 * Liefert die Aktie mit Index index in der alphabetischen Sortierung der
	 * Aktien.
	 * 
	 * @param index
	 *            Index der Aktie in alphabetischer Sortierung
	 * @return Aktie mit Index index in der alphabetischen Sortierung der Aktien
	 * 
	 * @throws IndexOutOfBoundsException
	 *             falls Index ungueltig
	 */
	public Share get(int index) {
		if (index < 0 || index >= shares.size()) {
			throw new IndexOutOfBoundsException("Illegal Index " + index);
		}
		int counter = 0;
		for (Share s : this) {
			if (counter++ == index) {
				return s;
			}
		}
		// wont happen
		return null;
	}

	/**
	 * F�gt der Aktiensammlung share hinzu falls diese noch nicht vorhanden.
	 * 
	 * @param share
	 *            Aktie die hinzugef�gt werden soll
	 * @return True wenn erfolgreich
	 */
	public boolean add(Share share) {
		if (shares.containsKey(share.getName())) {
			return false;
		}
		shares.put(share.getName(), share);
		return true;

	}

	/**
	 * Entfernt eine Aktie aus der Sammlung.
	 * 
	 * @param name
	 *            Name der Aktie die entfernt werden soll
	 * @return True wenn erfolgreich
	 */
	public boolean remove(String name) {
		return (shares.remove(name) == null) ? false : true;
	}

	/**
	 * Pr�ft ob eine Aktie mit dem Namen name in der Sammlung vorhanden ist
	 * 
	 * @param name
	 *            Name der Aktie
	 * @return True wenn Aktie vorhanden ist
	 */
	public boolean contains(String name) {
		return shares.containsKey(name);
	}

	/**
	 * Liefert die Anzahl der Aktien in der Sammlung
	 * 
	 * @return Anzahl der Aktien in der Sammlung
	 */
	public int size() {
		return shares.size();
	}

	/**
	 * Liefert eine Textrepr�sentation der gesamten Sammlung, im Format<br>
	 * 
	 * date;open;high;low;close<br>
	 * SHARE;name;;;<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * ...<br>
	 * SHARE;name;;;<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * ...
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("date;open;high;low;close");
		for (Share share : this) {
			sb.append(System.getProperty("line.separator"));
			sb.append(share.toString());
		}
		return sb.toString();
	}
}
