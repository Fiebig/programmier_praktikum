package jpp.marketanalysis.data;

import java.time.LocalDate;

import jpp.marketanalysis.analysis.DayComparator.DatasetDayComparator;
import jpp.marketanalysis.operationsContainer.FormatOperations;

/**
 * Fasst relevante Werte einer Aktie bezogen auf einen Tag zusammen
 * 
 * @author Alexander Fiebig
 * 
 */
public class Dataset {
	/**
	 * Datum auf das sich die Daten beziehen
	 */
	private final LocalDate date;
	/**
	 * Er�ffnungskurs
	 */
	private final double open;
	/**
	 * H�chstwert
	 */
	private final double high;
	/**
	 * Tiefstwert
	 */
	private final double low;
	/**
	 * Endkurs
	 */
	private final double close;

	/**
	 * Initialisiert eine Instanz
	 * 
	 * @param date
	 *            Tag auf den sich die Daten beziehen
	 * @param open
	 *            Er�ffnungswert
	 * @param high
	 *            H�chstwert
	 * @param low
	 *            Tiefstwert
	 * @param close
	 *            Schlusswert
	 */
	public Dataset(LocalDate date, double open, double high, double low,
			double close) {

		this.date = date;
		this.open = open;
		this.low = low;
		this.close = close;
		this.high = high;
	}

	/**
	 * Liefert den Tag auf den sich die Daten beziehen
	 * 
	 * @return Tag auf den sich die Daten beziehen
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * Liefert den Er�ffnungswert
	 * 
	 * @return Er�ffnungswert
	 */
	public double getOpen() {
		return open;
	}

	/**
	 * Liefert den Schlusswert
	 * 
	 * @return Schlusswert
	 */
	public double getClose() {
		return close;
	}

	/**
	 * Liefert den H�chstwert
	 * 
	 * @return H�chstwert
	 */
	public double getHigh() {
		return high;
	}

	/**
	 * Liefert den Tiefstwert
	 * 
	 * @return Tiefstwert
	 */
	public double getLow() {
		return low;
	}

	/**
	 * Vergleicht zwei Objekte Dataset Instanzen miteinander. Dabei werden alle
	 * Felder auf Gleichheit gepr�ft. Beim Vergleich der beiden Calendar-Objekte
	 * wird ausschlie�lich das Datum gepr�ft wird, nicht etwa die Uhrzeit.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Dataset other = (Dataset) obj;
		if (Double.doubleToLongBits(close) != Double
				.doubleToLongBits(other.close)) {
			return false;
		}
		if (Double.doubleToLongBits(high) != Double
				.doubleToLongBits(other.high)) {
			return false;
		}
		if (Double.doubleToLongBits(low) != Double.doubleToLongBits(other.low)) {
			return false;
		}
		if (Double.doubleToLongBits(open) != Double
				.doubleToLongBits(other.open)) {
			return false;
		}
		DatasetDayComparator ddc = new DatasetDayComparator();
		if (ddc.compare(this, other) != 0) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(close);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(high);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(low);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(open);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime
				* result
				+ (date.getYear() + date.getMonthValue() + date.getDayOfMonth());
		return result;
	}

	/**
	 * Liefert eine Textrepr�sentation des Datensatzes im Format<br>
	 * date;open;high;low;close
	 */
	@Override
	public String toString() {

		return FormatOperations.formatDate(date) + ";"
				+ FormatOperations.formatValue(this.open) + ";"
				+ FormatOperations.formatValue(this.high) + ";"
				+ FormatOperations.formatValue(this.low) + ";"
				+ FormatOperations.formatValue(this.close);
	}
}
