package jpp.marketanalysis.data;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeSet;

import jpp.marketanalysis.analysis.DayComparator.DatasetDayComparator;
import jpp.marketanalysis.operationsContainer.SortOperations;

/**
 * Modelliert eine Aktie mit ihren Kursdaten.
 * 
 * @author Alexander Fiebig
 * 
 */
public class Share {
	/**
	 * Name der Aktie
	 */
	private final String name;
	/**
	 * Datens�tze der Aktie
	 */
	private final List<Dataset> data;

	/**
	 * Initialisiert eine Instanz.
	 * 
	 * @param name
	 *            Name der Aktie
	 * @param data
	 *            Daten der Aktie
	 * @throws IllegalArgumentException
	 *             falls mehrere Datens�tze f�r dasselbe Datum vorhanden sind
	 */
	public Share(String name, Collection<Dataset> data) {

		this.data = new LinkedList<>();
		TreeSet<Dataset> ts = new TreeSet<>(new DatasetDayComparator());
		for (Dataset temp : data) {
			if (ts.add(temp) == false) {
				throw new IllegalArgumentException(
						"Mehrere Datens�tze f�r dasselbe Datum in " + name
								+ " vorhanden!");
			}
			this.data.add(temp);
		}
		this.name = name;
	}

	/**
	 * Liefert den Namen der Aktie
	 * 
	 * @return Name der Aktie
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Daten der Aktie.
	 * 
	 * @return Daten der Aktie.
	 */
	public List<Dataset> getData() {
		return data;
	}

	/**
	 * Vergleicht zwei Aktien anhand ihrer Namen
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Share other = (Share) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Liefert eine Textrepr�sentation der Aktie im Format<br>
	 * 
	 * SHARE;name;;;<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * date;open;high;low;close<br>
	 * ...
	 */
	@Override
	public String toString() {
		SortOperations.sortDataByDate(false, data);

		StringBuilder sb = new StringBuilder("SHARE;" + this.name + ";;;");
		for (ListIterator<Dataset> j = data.listIterator(); j.hasNext();) {
			sb.append(System.getProperty("line.separator"));
			sb.append(j.next().toString());
		}
		return sb.toString();
	}

}