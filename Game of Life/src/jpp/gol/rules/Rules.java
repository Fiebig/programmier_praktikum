package jpp.gol.rules;

import jpp.gol.model.CellState;

/**
 * Die Einf�hrung der Rules Schnittstelle erlaubt es, die grundlegenden
 * Spielregeln, n�mlich den Wert eines Feldes in der n�chsten Iteration in
 * Abh�ngigkeit der Anzahl seiner Nachbarn, schnell und einfach auszutauschen.
 * 
 * @author Alexander Fiebig
 * 
 */
public interface Rules {
	/**
	 * Returns the state of a field in the next iteration based on the living
	 * neighbours and the current state.
	 * 
	 * @param numberOfNeighbors
	 *            the number of living neighbors.
	 * @param currentValue
	 *            the current state.
	 * 
	 * @return the new state according to the rules.
	 **/
	public CellState nextState(int numberOfNeighbors, CellState currentValue);
}
