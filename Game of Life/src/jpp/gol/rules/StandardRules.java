package jpp.gol.rules;

import jpp.gol.model.CellState;

/**
 * Die Klasse StandardRules implementiert die Schnittstelle Rules mit den
 * Standardregeln von Conway's Game of Life: Eine tote Zelle mit genau drei
 * lebenden Nachbarn wird in der Folgegeneration neu geboren. Lebende Zellen mit
 * weniger als zwei lebenden Nachbarn sterben in der Folgegeneration an
 * Einsamkeit. Eine lebende Zelle mit zwei oder drei lebenden Nachbarn bleibt in
 * der Folgegeneration lebend. Lebende Zellen mit mehr als drei lebenden
 * Nachbarn sterben in der Folgegeneration an Überbevölkerung.
 * 
 * @author Alexander Fiebig
 * 
 */
public class StandardRules implements Rules {
	/**
	 * @throws IllegalArgumentException
	 *             falls numberOfNeighbors nicht im Bereich des Moeglichen ist
	 */
	@Override
	public CellState nextState(int numberOfNeighbors, CellState currentValue) {
		if (numberOfNeighbors > 8 || numberOfNeighbors < 0) {
			throw new IllegalArgumentException(
					"Die Anzahl der (lebenden) Nachbarn einer Zelle kann nicht >8 oder <0 sein");
		}

		if (currentValue == CellState.DEAD && numberOfNeighbors == 3) {// Rule
																		// 1
			return CellState.ALIVE;
		}
		if (currentValue == CellState.ALIVE && numberOfNeighbors < 2) {// Rule
																		// 2
			return CellState.DEAD;
		}
		if (currentValue == CellState.ALIVE
				&& (numberOfNeighbors == 2 || numberOfNeighbors == 3)) {// Rule
																		// 3
			return CellState.ALIVE;
		}
		if (currentValue == CellState.ALIVE && numberOfNeighbors > 3) {// Rule
																		// 4
			return CellState.DEAD;
		}
		if (currentValue == CellState.DEAD) {
			return CellState.DEAD;
		} else {
			return CellState.ALIVE;
		}

	}

}
