package jpp.gol.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import jpp.gol.model.CellState;
import jpp.gol.model.IllegalWorldSizeException;
import jpp.gol.model.World;

/**
 * Die Klasse StandardWorldLoader unterst�tzt das Laden von und das Speicher in
 * ein einfaches Textformat.
 * 
 * @author Alexander Fiebig
 * 
 */
public class StandardWorldLoader implements WorldLoader {
	/**
	 * Liest eine Welt, die im Textformat<br>
	 * [Breite der Zeile]x[Anzahl der Zeilen]<br>
	 * [Welt (wie von World.toString() ausgegeben)]<br>
	 * gespeichert ist, von in ein. Tritt ein Fehler im Format auf, wird eine
	 * IOException geworfen.
	 * 
	 * @return Entsprechendes World-Objekt
	 */
	@Override
	public World load(InputStream in) throws IOException {
		try {
			InputStreamReader file = new InputStreamReader(in);
			BufferedReader data = new BufferedReader(file);
			String zeile = data.readLine().trim();
			String[] headLine = zeile.split("x");
			int length = Integer.parseInt(headLine[0]);
			int height = Integer.parseInt(headLine[1]);

			World w = new World(length, height);

			for (int i = 0; i < height; i++) {
				zeile = data.readLine().trim();
				formatTest(zeile, length);
				for (int j = 0; j < length; j++) {
					char cellState = zeile.charAt(j);
					if (cellState == '1') {
						w.set(j, i, CellState.ALIVE);
					} else {
						w.set(j, i, CellState.DEAD);
					}
				}
			}
			if (data.readLine() != null) {
				throw new IOException("Ueberschuessige DataLine(s)");
			}
			return w;
		} catch (NumberFormatException e) {
			throw new IOException("Ung�ltige HeadLine");
		} catch (NullPointerException e) {
			throw new IOException(
					"Unvollstaendige Eingabe: Fehlende Data- bzw. HeadLine");
		} catch (IllegalWorldSizeException e) {
			throw new IOException(
					"Ung�ltige HeadLine: L�nge und Breite der Welt m�ssen >0 sein!");
		}

	}

	private void formatTest(String zeile, int length) throws IOException {

		if (zeile.length() != length) {
			throw new IOException("Ung�ltige DataLine");
		}
		for (int j = 0; j < length; j++) {
			char c = zeile.charAt(j);
			if (c != '1' && c != '0') {
				throw new IOException("Ung�ltige DataLine");
			}

		}
	}

	/**
	 * Schreibt world auf out im Textformat<br>
	 * [Breite der Zeile]x[Anzahl der Zeilen]<br>
	 * [Welt (wie von World.toString() ausgegeben)]
	 * 
	 * @param world
	 *            Gegebene Welt
	 * @param out
	 *            Outputstream auf den geschrieben werden soll
	 */
	@Override
	public void save(World world, OutputStream out) throws IOException {
		try {
			String headLine = world.getLength() + "x" + world.getHeight()
					+ System.getProperty("line.separator");
			out.write(headLine.getBytes());
			out.flush();
			String data = world.toString();
			out.write(data.getBytes());
			out.flush();
			out.close();
		} catch (NullPointerException e) {
			throw new IOException(e.getMessage());
		}
	}
}
