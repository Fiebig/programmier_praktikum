package jpp.gol.gui;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import jpp.gol.control.ThreadingGameController;
import jpp.gol.model.CellState;
import jpp.gol.model.World;

public class WorldGrid extends GridPane {

	private final double gridSize;
	/**
	 * Canvas, die die Welt darstellen. Ein Canvas pro CellState.
	 */
	private Canvas[][] canvasArray;

	public WorldGrid(double size, World w) {
		this.gridSize = size;
		setMinHeight(gridSize);
		setMaxHeight(gridSize);
		setMinWidth(gridSize);
		setMaxWidth(gridSize);
		setAlignment(Pos.CENTER);
		initForWorld(w);
	}

	public void initForWorld(World w) {
		getChildren().clear();
		canvasArray = new Canvas[w.getLength()][w.getHeight()];
		double squareSize = computeSquareSize(w);
		for (int i = 0; i < w.getLength(); i++) {
			for (int j = 0; j < w.getHeight(); j++) {
				Canvas c = new Canvas(squareSize, squareSize);
				canvasArray[i][j] = c;
				add(canvasArray[i][j], 0 + i, 0 + j, 1, 1);
			}
		}
		setHgap(computeGapSize(w));
		setVgap(computeGapSize(w));
		drawWorld(w);
	}

	/**
	 * Zeichnet die Quader auf die Canvas, die die Welt darstellen.
	 * 
	 * @param currentWorld
	 *            Die aktuelle Welt
	 */
	public void drawWorld(World currentWorld) {
		for (int i = 0; i < currentWorld.getLength(); i++) {
			for (int j = 0; j < currentWorld.getHeight(); j++) {
				if (currentWorld.getField(i, j) == CellState.ALIVE) {
					canvasArray[i][j].getGraphicsContext2D().setFill(
							Color.GREEN);
				} else {
					canvasArray[i][j].getGraphicsContext2D().setFill(Color.RED);
				}

				canvasArray[i][j].getGraphicsContext2D().fillRoundRect(0, 0,
						canvasArray[i][j].getWidth(),
						canvasArray[i][j].getHeight(), 0, 0);
			}
		}
	}

	/**
	 * Setzt die Maus-Events auf die Canvas um die CellStates ver�ndern zu
	 * k�nnen.
	 * 
	 * @param tgc
	 *            Controller um Ver�nderungen durchzuf�hren
	 */
	public void setCanvasActions(final ThreadingGameController tgc) {
		for (int i = 0; i < tgc.getWorld().getLength(); i++) {
			for (int j = 0; j < tgc.getWorld().getHeight(); j++) {
				final int x = i;
				final int y = j;
				canvasArray[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent t) {
								if (t.getClickCount() == 1) {
									tgc.changeState(x, y);
								}
							}
						});
			}
		}
	}

	private double computeSquareSize(World w) {
		double verf�gbarerPlatz = (gridSize / 100) * 70;

		if (w.getLength() >= w.getHeight()) {
			return verf�gbarerPlatz / w.getLength();
		} else {
			return verf�gbarerPlatz / w.getHeight();
		}
	}

	private double computeGapSize(World w) {
		double verf�gbarerPlatz = (gridSize / 100) * 30;

		if (w.getLength() >= w.getHeight()) {
			return verf�gbarerPlatz / w.getLength();
		} else {
			return verf�gbarerPlatz / w.getHeight();
		}
	}

}
