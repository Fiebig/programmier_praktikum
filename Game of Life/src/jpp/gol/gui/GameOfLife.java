package jpp.gol.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jpp.gol.control.ThreadingGameController;
import jpp.gol.io.StandardWorldLoader;
import jpp.gol.io.WorldLoader;
import jpp.gol.logic.GameLogic;
import jpp.gol.logic.StandardGameLogic;
import jpp.gol.logic.WorldChangedListener;
import jpp.gol.model.IllegalWorldSizeException;
import jpp.gol.model.World;
import jpp.gol.rules.Rules;
import jpp.gol.rules.StandardRules;

/**
 * Startet die graphische Benutzeroberfläche von GameOfLife.
 * 
 * @author Alexander Fiebig
 * 
 */
public class GameOfLife extends Application implements WorldChangedListener {
	/**
	 * ThreadingGameController-Instanz mit der das Spiel gesteuert wird.
	 */
	private final ThreadingGameController tgc;
	/**
	 * WorldLoader zum Einlesen einer Welt.
	 */
	private final WorldLoader wl;
	private final WorldGrid worldGrid;

	/**
	 * Erstellt ein neues Game of Life. Setzt dabei die Attribute mithilfe der
	 * Standard-Implementierungen. Es wird eine 10x10 Startwelt erstellt.
	 */
	public GameOfLife() {
		World w = new World();
		Rules sgr = new StandardRules();
		GameLogic sgl = new StandardGameLogic(w, sgr);
		tgc = new ThreadingGameController(sgl);
		wl = new StandardWorldLoader();
		tgc.getDecorator().addWorldChangedListener(this);
		this.worldGrid = new WorldGrid(700, w);
		worldGrid.setCanvasActions(tgc);
	}

	/**
	 * Wird vom GameThread aufgerufen um dem GUI Veränderungen an der Welt
	 * mitzuteilen.
	 * 
	 * @param w
	 *            Veränderte Welt
	 */
	@Override
	public void onChange(final World w) {
		if (Platform.isFxApplicationThread() == true) {
			worldGrid.drawWorld(w);
		}
		else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					worldGrid.drawWorld(w);
				}
			});
		}
	}

	@Override
	public void start(final Stage primaryStage) {
		primaryStage.setTitle("Game of Life");

		final Button btn = new Button();
		btn.setText("Start Game");
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tgc.start();
			}
		});

		final Button btn2 = new Button();
		btn2.setText("Stop Game");
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				tgc.stop();
			}
		});

		final Text tf1 = new Text("Gamespeed:");
		tf1.setFont(Font.font("Arial", FontWeight.BOLD, 16));

		/*
		 * Slider mit dem sich die Spielgeschwindigkeit im Bereich von 50ms bis
		 * 2000ms einstellen laesst
		 */
		final Slider speedSlider = new Slider(50, 2000, 200);
		speedSlider.setMaxSize(250, 30);
		speedSlider.setMinSize(250, 30);
		speedSlider.setShowTickLabels(true);
		speedSlider.setMajorTickUnit(1950);

		speedSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov,
					Number old_val, Number new_val) {

				tgc.setTimeBetweenSteps(new_val.intValue());
			}
		});

		final Text tf2 = new Text("New Worldsize:");
		tf2.setFont(Font.font("Arial", FontWeight.BOLD, 16));

		final TextField worldTextField = new TextField();
		worldTextField.setText("Format: [length]x[height]");
		worldTextField.setMaxSize(200, 30);
		worldTextField.setMinSize(200, 30);
		worldTextField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				try {
					String text = worldTextField.getText();
					String[] headLine = text.split("x");
					int length = Integer.parseInt(headLine[0]);
					int height = Integer.parseInt(headLine[1]);
					World w = new World(length, height);
					worldGrid.initForWorld(w);
					tgc.reset(w);
					worldGrid.setCanvasActions(tgc);
				}
				catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
					worldTextField.setText("Falsches Format!");
				}
				catch (IllegalWorldSizeException ex) {
					worldTextField.setText("Illegal Worldsize!");
				}
			}
		});

		final Button loadButton = new Button("Load World");
		loadButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				try {
					Stage stage = new Stage();
					FileChooser fileChooser = new FileChooser();
					File file = fileChooser.showOpenDialog(stage);
					if (file != null) {
						InputStream in = new FileInputStream(file);
						World w = wl.load(in);
						worldGrid.initForWorld(w);
						tgc.reset(w);
						worldGrid.setCanvasActions(tgc);
					}
				}
				catch (IOException ex) {
					System.err.println("Ungültige Datei: " + ex.getMessage());
				}
			}
		});

		final Button saveButton = new Button("Save World");

		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				try {
					Stage stage = new Stage();
					FileChooser fileChooser = new FileChooser();
					// Set extension filter
					FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
							"TXT files (*.txt)", "*.txt");
					fileChooser.getExtensionFilters().add(extFilter);
					// Create or select file
					File selectedFile = fileChooser.showSaveDialog(stage);
					if (selectedFile != null) {
						OutputStream out = new FileOutputStream(selectedFile);
						wl.save(tgc.getWorld(), out);
					}
				}
				catch (IOException ex) {
					System.err.println(ex.getMessage());
				}
			}
		});

		final HBox hbUnten = new HBox();
		hbUnten.setMinWidth(1300);
		hbUnten.setStyle("-fx-background-color: #336699;");
		hbUnten.setSpacing(20);
		hbUnten.setPadding(new Insets(10, 500, 10, 20));
		hbUnten.getChildren().addAll(tf1, speedSlider, tf2, worldTextField);
		HBox.setMargin(tf1, new Insets(0, 0, 0, 250));
		HBox.setMargin(tf2, new Insets(0, 0, 0, 30));

		final HBox hbOben = new HBox();
		hbOben.setMinWidth(1300);
		hbOben.setStyle("-fx-background-color: #336699;");
		hbOben.setSpacing(20);
		hbOben.setPadding(new Insets(10, 20, 10, 20));
		hbOben.getChildren().addAll(btn, btn2, loadButton, saveButton);
		HBox.setMargin(loadButton, new Insets(0, 0, 0, 800));

		final BorderPane root = new BorderPane();
		root.setStyle("-fx-background-color: #000000;");
		root.setBottom(hbUnten);
		root.setTop(hbOben);
		root.setCenter(worldGrid);

		primaryStage.setScene(new Scene(root, 1300, 975));
		primaryStage.sizeToScene();
		primaryStage.setResizable(false);
		primaryStage.show();
	}

}
