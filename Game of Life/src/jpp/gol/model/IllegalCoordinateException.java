package jpp.gol.model;

@SuppressWarnings("serial")
public class IllegalCoordinateException extends RuntimeException {

	public IllegalCoordinateException() {
	}

	public IllegalCoordinateException(String message) {
		super(message);

	}

}
