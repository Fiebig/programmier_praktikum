package jpp.gol.model;

/**
 * Es gibt zwei Zust�nde f�r Felder im Game of Life: tot und lebendig. Die
 * Enumeration CellState stellt den Zustand eines Feldes unseres Spiels dar. Sie
 * enth�lt die Werte DEAD und ALIVE.
 * 
 * @author Alexander Fiebig
 * 
 */
public enum CellState {

	// Zust�nde der Zellen
	DEAD, ALIVE;

	/**
	 * Wandelt einen Booleschen Wert in einen CellState um. Gibt DEAD zur�ck,
	 * wenn b false ist und ALIVE, wenn b true ist.
	 * 
	 * @param b
	 *            Boolean Wert
	 * @return Entsprechender CellState
	 */
	public static CellState fromBoolean(boolean b) {
		if (b == true) {
			return CellState.ALIVE;
		}
		return CellState.DEAD;

	}

	/**
	 * Gibt CellState als String zur�ck
	 * 
	 * @return Entsprechender CellState als String
	 */
	public String getState() {
		if (this == DEAD) {
			return "DEAD";
		} else {
			return "ALIVE";
		}
	}

}
