package jpp.gol.model;

import java.util.Arrays;

/**
 * Die Klasse World repr�sentiert und verwaltet das Spielfeld von Game of Life.
 * World nimmt im MVC-Modell den Platz des Modells ein. Es enth�lt alle
 * darzustellenden Daten, ist aber von der kompletten Spiellogik unabh�ngig.
 * 
 * @author Alexander Fiebig
 * 
 */
public class World implements Cloneable {
	/**
	 * L�nge der Welt
	 */
	private final int length;
	/**
	 * H�he der Welt
	 */
	private final int height;
	/**
	 * Felder aus denen die Welt besteht
	 */
	private CellState[][] stateOfCells;

	/**
	 * Erstellt eine 10x10 Welt, die nur tote Felder enth�lt.
	 */
	public World() {
		length = 10;
		height = 10;
		stateOfCells = new CellState[10][10];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				stateOfCells[i][j] = CellState.DEAD;
			}
		}
	}

	/**
	 * Erstellt eine Welt mit gegebener Breite und H�he, die nur tote Felder
	 * enth�lt.
	 * 
	 * @param length
	 *            L�nge der Welt
	 * @param height
	 *            H�he der Welt
	 * 
	 * @throws IllegalWorldSizeException
	 *             falls length oder height <= 0
	 */
	public World(int length, int height) {
		if (length <= 0 || height <= 0) {
			throw new IllegalWorldSizeException(
					"L�nge und Breite der Welt m�ssen >0 sein");
		}
		this.length = length;
		this.height = height;
		stateOfCells = new CellState[length][height];
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < height; j++) {
				stateOfCells[i][j] = CellState.DEAD;
			}
		}
	}

	/**
	 * Gibt die Anzahl der lebenden Nachbarn f�r das Feld mit den Koordinaten x
	 * und y zur�ck. Die Welt bildet einen Torus, d.h. wenn Sie �ber den
	 * Feldrand hinaus kommen, geht es auf der entgegen gesetzten Seite weiter.
	 * 
	 * @param x
	 *            X-Koordinate des Feldes
	 * @param y
	 *            Y-Koordinate des Feldes
	 * 
	 * @throws IllegalCoordinateException
	 *             falls die Koordinate nicht Teil der Welt ist
	 * @return Anzahl der lebenden Nachbarn
	 */
	public int countNeighbors(int x, int y) {
		if ((x >= length || x < 0) || (y >= height || y < 0)) {
			throw new IllegalCoordinateException(
					"Es existiert in der Welt kein Feld mit den Koordinaten ("
							+ x + "/" + y + ")");
		}
		int counter = 0;
		int temp = 0;
		int i = x - 2;
		while (temp < 3) {
			temp++;
			i++;
			if (i < 0) {
				i = length - 1;
			}
			if (i >= length) {
				i = 0;
			}
			int temp2 = 0;
			int j = y - 2;
			while (temp2 < 3) {
				temp2++;
				j++;
				if (j < 0) {
					j = height - 1;
				}
				if (j >= height) {
					j = 0;
				}
				if (stateOfCells[i][j] == CellState.ALIVE && (i != x || j != y)) {
					counter++;
				}
			}
		}
		return counter;
	}

	/**
	 * Liefert die H�he der Welt.
	 * 
	 * @return H�he der Welt
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Liefert die L�nge der Welt.
	 * 
	 * @return L�nge der Welt
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Wei�t der Koordinate (x,y) den Zustand value zu.
	 * 
	 * @param x
	 *            X-Koordinate des Feldes
	 * @param y
	 *            Y-Koordinate des Feldes
	 * @param value
	 *            Zustand der dem Feld zugewiesen werden soll
	 * 
	 * @throws IllegalCoordinateException
	 *             falls die Koordinate nicht Teil der Welt ist
	 */
	public void set(int x, int y, CellState value) {
		if (x >= length || x < 0 || y >= height || y < 0) {
			throw new IllegalCoordinateException(
					"Es existiert in der Welt kein Feld mit den Koordinaten ("
							+ x + "/" + y + ")");
		}
		if (value == CellState.DEAD) {
			stateOfCells[x][y] = CellState.DEAD;
		} else {
			stateOfCells[x][y] = CellState.ALIVE;
		}

	}

	/**
	 * Liefert den Zustand des Feldes mit den Koordinaten x und y.
	 * 
	 * @param x
	 *            X-Koordinate des Feldes
	 * @param y
	 *            Y-Koordinate des Feldes
	 * 
	 * @throws IllegalCoordinateException
	 *             falls die Koordinate nicht Teil der Welt ist
	 * @return Zustand des Feldes
	 */
	public CellState getField(int x, int y) {
		if (x >= length || x < 0 || y >= height || y < 0) {
			throw new IllegalCoordinateException(
					"Es existiert in der Welt kein Feld mit den Koordinaten ("
							+ x + "/" + y + ")");
		}
		return stateOfCells[x][y];
	}

	public CellState[][] getStateOfCells() {

		return stateOfCells;
	}

	/**
	 * Gibt die Welt in einem einfachen Textformat aus. Dabei werden lebende
	 * Zellen durch eine 1, tote durch eine 0 repr�sentiert.
	 */
	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < height; i++) {
			if (i != 0) {
				sb.append(System.getProperty("line.separator"));
			}
			for (int j = 0; j < length; j++) {
				if (stateOfCells[j][i] == CellState.ALIVE) {
					sb.append(1);
				} else {
					sb.append(0);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Vergleicht zwei World-Objekte anhand deren stateOfCells Attribut.
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		if (!(Arrays.deepEquals(this.stateOfCells, ((World) obj).stateOfCells))) {
			return false;
		}

		return true;
	}

	/**
	 * Da die equals Methode der Klasse Object �berschrieben wurde, wird nun
	 * auch die hashCode Methode �berschrieben, da die hashCode Methode, die von
	 * der Klasse Object geerbt wird, nicht mehr zu der neuen equals Methode
	 * passt.
	 */
	@Override
	public int hashCode() {
		int hc = 23;// willk�rlicher Anfangswert
		int hashMultiplier = 59;// kleine Primzahl
		int temp = ((stateOfCells == null) ? 0 : Arrays
				.deepHashCode(stateOfCells));
		hc = hc * hashMultiplier + temp;
		return hc;

	}

	/**
	 * Erstellt eine Kopie der Welt.
	 * 
	 * @return Geklontes World-Objekt
	 */
	@Override
	public World clone() {

		World w = new World(this.length, this.height);

		w.stateOfCells = this.stateOfCells.clone();
		for (int i = 0; i < this.length; i++) {
			w.stateOfCells[i] = this.stateOfCells[i].clone();
		}
		return w;
	}
}
