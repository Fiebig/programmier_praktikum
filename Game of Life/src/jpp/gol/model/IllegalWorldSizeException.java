package jpp.gol.model;

@SuppressWarnings("serial")
public class IllegalWorldSizeException extends RuntimeException {

	public IllegalWorldSizeException() {
	}

	public IllegalWorldSizeException(String message) {
		super(message);
	}

}
