package jpp.gol.control;

import jpp.gol.logic.GameLogic;
import jpp.gol.logic.ObservableGameLogicDecorator;
import jpp.gol.model.World;

/**
 * Die Klasse startet das Spiel mit Hilfe der GameLogic - Klasse in einem
 * separaten Thread und bietet Methoden um die Ausf�hrung zu manipulieren.
 * 
 * @author Alexander Fiebig
 * 
 */
public class ThreadingGameController {

	/**
	 * Thread, der das Spiel ausf�hrt.
	 */
	private GameThread gameThread;
	/**
	 * Zu nutzender Decorator.
	 */
	private final ObservableGameLogicDecorator ogld;

	/**
	 * Initialisiert ein Objekt mit der entsprechenden Spiellogik. Setzt
	 * au�erdem die Wartezeit zwischen zwei Iterationen auf 200ms.
	 * 
	 * @param logic
	 *            Zugrunde liegende Spiellogik
	 */
	public ThreadingGameController(GameLogic logic) {
		this.ogld = new ObservableGameLogicDecorator(logic);
		this.gameThread = new GameThread(200);
		gameThread.setDaemon(true);
	}

	/**
	 * Liefert die momentane Thread-Wartezeit.
	 * 
	 * @return Thread-Wartezeit in ms
	 */
	public long getWarteZeit() {
		return gameThread.sleepTime;
	}

	/**
	 * Liefert den Decorator.
	 * 
	 * @return Genutzter Decorator
	 */
	public ObservableGameLogicDecorator getDecorator() {
		return ogld;
	}

	/**
	 * Liefert die aktuelle Welt.
	 * 
	 * @return Aktuelle Welt
	 */
	public World getWorld() {
		return ogld.getWorld();
	}

	/**
	 * Startet das aktuelle Spiel in einem separaten Thread.
	 */
	public void start() {
		if (gameThread.getState() == Thread.State.NEW) {
			gameThread.start();
		} else if (gameThread.getState() == Thread.State.TERMINATED) {
			gameThread = new GameThread(getWarteZeit());
			gameThread.start();
		}

	}

	/**
	 * Stoppt den Spielthread, falls er l�uft.
	 */
	public void stop() {
		if (gameThread.isAlive()) {
			gameThread.interrupt();
		}
	}

	/**
	 * Teilt dem Spielthread mit, dass das Spiel anhalten soll.
	 */
	public void pause() {
		if (gameThread.isAlive()) {
			gameThread.shouldWait = true;
		}
	}

	/**
	 * Teilt dem Spielthread mit, dass ein pausiertes Spiel weiterlaufen soll.
	 */
	public void resume() {
		if (isPaused()) {
			gameThread.shouldWait = false;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/**
	 * Pr�ft ob der Spielthread pausiert.
	 */
	public boolean isPaused() {
		if (gameThread.getState() == Thread.State.WAITING) {
			return true;
		}
		return false;
	}

	/**
	 * Teilt dem Spielthread mit, wie lange zwischen den einzelnen Iterationen
	 * gewartet werden soll.
	 * 
	 * @param duration
	 *            Gew�nschte Wartezeit in ms
	 */
	public void setTimeBetweenSteps(long duration) {
		gameThread.sleepTime = duration;
	}

	/**
	 * Stoppt den aktuellen Spielthread, falls er l�uft. Setzt dann eine neue
	 * Welt in der GameLogic.
	 * 
	 * @param world
	 *            Neue Welt
	 */
	public void reset(World world) {
		if (gameThread.isAlive()) {
			gameThread.interrupt();
		}
		ogld.setWorld(world);
	}

	/**
	 * Teilt der Spiellogik mit, dass sich ein Zustand ver�ndern soll. Siehe
	 * auch: GameLogic -> changeState. Dies ist nur dann m�glich, wenn das Spiel
	 * gestoppt ist.
	 * 
	 * @param x
	 *            X-Koordinate des Feldes
	 * @param y
	 *            Y-Koordinate des Feldes
	 * 
	 * @throws IllegalStateException
	 *             falls das Spiel nicht gestoppt ist
	 */
	public void changeState(int x, int y) {
		if (gameThread.isAlive()) {
			throw new IllegalStateException(
					"Der Gamethread (dasSpiel) muss f�r eine Zustands�nderung gestoppt worden sein!");
		}

		ogld.changeState(x, y);

	}

	/**
	 * Innere Klasse von ThreadingGameController, die den Thread implementiert,
	 * in dem das Spiel l�uft.
	 * 
	 * @author Alexander Fiebig
	 * 
	 */
	private class GameThread extends Thread {
		private long sleepTime;
		private boolean shouldWait;
		/**
		 * Erzeugt einen neuen Spielthread.
		 * 
		 * @param sleepTime
		 *            Wartezeit zwischen den einzelnen Iterationen
		 */
		private GameThread(long sleepTime) {
			this.sleepTime = sleepTime;
			this.shouldWait = false;
		}

		@Override
		public void run() {
			synchronized (ThreadingGameController.this) {
				while (this.isInterrupted() == false) {
					try {
						ThreadingGameController.this.ogld.step();
						if (shouldWait == true) {
							ThreadingGameController.this.wait();
						}
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						this.interrupt();
						System.out.println("Unterbrechung in sleep()");
					}
				}
			}
		}
	}

}
