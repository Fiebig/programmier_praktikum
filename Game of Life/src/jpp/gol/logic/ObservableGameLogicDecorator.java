package jpp.gol.logic;

import java.util.LinkedList;

import jpp.gol.model.World;

/**
 * Die Klasse ObservableGameLogicDecorator implementiert die Schnittstelle
 * GameLogic und nutzt das Dekorierer-Muster um einer beliebigen Implementierung
 * von GameLogic Verhalten hinzuzuf�gen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class ObservableGameLogicDecorator implements GameLogic {

	/**
	 * Zu nutzende GameLogic
	 */
	private final GameLogic gl;
	/**
	 * Liste der Listener, die �ber �nderungen an der Welt informiert werden
	 * m�ssen
	 */
	private final LinkedList<WorldChangedListener> wcl;

	/**
	 * Initialisiert eine neue Instanz, die alle Aufrufe an delegate
	 * weiterleitet.
	 * 
	 * @param delegate
	 *            Die zu verwendende GameLogic
	 * 
	 * @throws NullPointerException
	 *             falls der Parameter delegate den Wert null hat
	 */
	public ObservableGameLogicDecorator(GameLogic delegate) {
		if (delegate == null) {
			throw new NullPointerException();
		}
		this.wcl = new LinkedList<WorldChangedListener>();
		this.gl = delegate;

	}

	/**
	 * Liefert die GameLogic
	 * 
	 * @return Genutzte GameLogic
	 */
	public GameLogic getGameLogic() {
		return gl;
	}

	/**
	 * Registriert einen Listener, sodass er in Zukunft �ber �nderungen an der
	 * Welt informiert wird. Ein Listener darf nur einmal registriert werden.
	 * 
	 * @param listener
	 *            Zu registrierender Listener
	 */
	public void addWorldChangedListener(WorldChangedListener listener) {
		if (wcl.contains(listener) == false) {
			wcl.add(listener);
		}
	}

	/**
	 * Entfernt einen Listener, sodass er in Zukunft nicht mehr �ber �nderungen
	 * an der Welt informiert wird.
	 * 
	 * @param listener
	 *            Zu entfernender Listener
	 */
	public void removeWorldChangedListener(WorldChangedListener listener) {
		wcl.remove(listener);
	}

	/**
	 * Ruft step() von gl auf und teilt allen Listeners die dadurch verursachten
	 * Ver�nderungen an der Welt mit.
	 */
	@Override
	public void step() {
		gl.step();
		WorldChangedListener[] temp = wcl.toArray(new WorldChangedListener[0]);
		for (int i = 0; i < temp.length; i++) {
			temp[i].onChange(getWorld());
		}

	}

	/**
	 * Ruft setWorld() von gl auf und teilt allen Listeners die dadurch
	 * verursachten Ver�nderungen an der Welt mit.
	 */
	@Override
	public void setWorld(World world) {
		gl.setWorld(world);
		WorldChangedListener[] temp = wcl.toArray(new WorldChangedListener[0]);
		for (int i = 0; i < temp.length; i++) {
			temp[i].onChange(getWorld());
		}

	}

	/**
	 * Liefert die aktuelle Welt.
	 */
	@Override
	public World getWorld() {
		return gl.getWorld();
	}

	/**
	 * Ruft changeState() von gl auf und teilt allen Listeners die dadurch
	 * verursachten Ver�nderungen an der Welt mit.
	 */
	@Override
	public void changeState(int x, int y) {
		gl.changeState(x, y);
		WorldChangedListener[] temp = wcl.toArray(new WorldChangedListener[0]);
		for (int i = 0; i < temp.length; i++) {
			temp[i].onChange(getWorld());
		}

	}

}
