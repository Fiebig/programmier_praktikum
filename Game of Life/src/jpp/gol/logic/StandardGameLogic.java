package jpp.gol.logic;

import jpp.gol.model.CellState;
import jpp.gol.model.World;
import jpp.gol.rules.Rules;

/**
 * Implementiert die Schnittstelle GameLogic.
 * 
 * @author Alexander Fiebig
 * 
 */
public class StandardGameLogic implements GameLogic {
	private World w;
	private final Rules r;

	/**
	 * Initialisiert eine neue Instanz mit einer Welt und den zu verwendenden
	 * Regeln.
	 * 
	 * @param world
	 *            Zugrunde liegende Welt
	 * @param rules
	 *            Zu verwendente Regeln
	 * 
	 * @throws NullPointerException
	 *             falls einer der beiden Parameter world oder rules den Wert
	 *             null hat
	 */
	public StandardGameLogic(World world, Rules rules) {
		if (world == null || rules == null) {
			throw new NullPointerException();
		}
		this.w = world;
		this.r = rules;
	}

	@Override
	public void step() {
		World temp = w.clone();
		for (int i = 0; i < temp.getLength(); i++) {
			for (int j = 0; j < temp.getHeight(); j++) {
				if (temp.getField(i, j) == CellState.DEAD) {
					w.set(i, j, r.nextState(temp.countNeighbors(i, j),
							CellState.DEAD));
				} else {
					w.set(i, j, r.nextState(temp.countNeighbors(i, j),
							CellState.ALIVE));
				}

			}
		}

	}

	@Override
	public World getWorld() {
		return w;
	}

	@Override
	public void setWorld(World world) {
		this.w = world;
	}
	/**
	 * @throws IllegalCoordinateException
	 *             falls die Koordinate nicht Teil der Welt ist
	 */
	@Override
	public void changeState(int x, int y) {
		if (w.getField(x, y) == CellState.DEAD) {
			w.set(x, y, CellState.ALIVE);
		} else {
			w.set(x, y, CellState.DEAD);
		}
	}
}